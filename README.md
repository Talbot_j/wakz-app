### Requirements
-``npm``
### Windows installation
-``git clone https://gitlab.com/Talbot_j/wakz-app.git``

-``cd wakz-app``

-``npm i --legacy-peer-deps``

### IOs installation
-``git clone https://gitlab.com/Talbot_j/wakz-app.git``

-``cd wakz-app``

-``npm i --legacy-peer-deps``

-``cd ios``

-``pod install``

### Android Deployment
``npx react-native run-android``

### iOS Deployment
``open XCode``

``Choose device and run``
