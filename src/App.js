import AppWithStore from './AppWithStore';
import Splash from './views/splash';

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {persistor, store} from './store/store';
import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Colors} from './views/stylesVar';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<Splash />} persistor={persistor}>
          <SafeAreaView style={styles.container}>
            <AppWithStore />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.base,
  },
});
