export function sectionFromContactsAndGroups(user, contacts, groups) {
  const reducer = (re, o) => {
    let firstLetter = (o.firstName ? o.firstName[0] : o.title[0]).toUpperCase();
    let existObj = re.find(obj => obj.title === firstLetter);
    if (existObj) {
      existObj.data.push(o);
    } else {
      re.push({
        title: firstLetter,
        data: [o],
      });
    }
    return re;
  };

  let contactsReduced = contacts.reduce(reducer, []);
  if (!groups) {
    return contactsReduced;
  }
  return groups.reduce((re, o) => {
    if (!o.title) {
      return re;
    }
    return reducer(re, o);
  }, contactsReduced);
}
