import {format, parse} from 'date-fns';

export function prettyDate(date) {
  let _date = parse(date.date, 'yyyy-MM-dd HH:mm:ss.SSSSSS', new Date());
  return format(_date, 'dd/MM/yy');
}
export function prettyDateAndHours(date) {
  let _date = parse(date.date, 'yyyy-MM-dd HH:mm:ss.SSSSSS', new Date());
  return format(_date, 'dd/MM/yy H:mm');
}
