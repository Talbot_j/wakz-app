import call from '../apiFactory';

export function postContactsInfos(contacts, authorization) {
  return call(
    'POST',
    'contact',
    {
      phoneNumbers: contacts,
    },
    authorization,
  );
}

export function getContactsInfos(authorization) {
  return call('GET', 'contact', null, authorization);
}
