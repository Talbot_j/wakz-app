import call from '../apiFactory';

export function postPhone(phone) {
  return call('POST', 'login', {
    phone: phone,
  });
}
export function postSmsCode(phone, smsCode) {
  return call('POST', 'login', {
    phone: phone,
    phoneAuth: smsCode,
  });
}

export function postProfileInfo(firstName, picture, authorization, dispatch) {
  return call(
    'POST',
    'user',
    {
      firstName: firstName,
      profilePicture: picture
        ? {
            data: picture.data,
            mime: picture.mime,
          }
        : {},
    },
    authorization,
    dispatch,
  );
}
