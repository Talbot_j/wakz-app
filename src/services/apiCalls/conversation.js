import call from '../apiFactory';

export function postWax(dest, message, position, authorization) {
  return call(
    'POST',
    'conversation',
    {
      dests: dest,
      message: message,
      position: position,
    },
    authorization,
  );
}

export function getConversation(authorization) {
  return call('GET', 'conversation', null, authorization);
}

export function postGroup(users, message, picture, authorization) {
  return call(
    'POST',
    'group',
    {
      users: users,
      title: message,
      picture: picture,
    },
    authorization,
  );
}

export function getGroup(authorization) {
  return call('GET', 'group', null, authorization);
}

export function postMessage(conversation, message, authorization) {
  return call(
    'POST',
    'message',
    {
      conversation: conversation,
      message: message,
    },
    authorization,
  );
}

export function getMessage(authorization) {
  return call('GET', 'message', null, authorization);
}

export function getUnlockedList(authorization, props) {
  return call('GET', 'unlock', null, authorization, props);
}

export function postUnlocked(conversationId, authorization) {
  return call(
    'POST',
    'unlock',
    {
      conversation: conversationId,
    },
    authorization,
  );
}
