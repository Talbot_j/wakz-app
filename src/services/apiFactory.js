// const URL = 'http://192.168.1.31/wax/'; // Local
const URL = 'https://dev.junecarter.fr/'; // DEV

export async function call(
  method = 'GET',
  endpoint = '',
  content = null,
  authorization = '',
  dispatch = null,
) {
  let init = {
    method: method,
    headers: {
      'Content-Type': 'Application/json',
    },
  };
  if (authorization) {
    init.headers.Authorization = authorization;
  }
  if (content) {
    init.body = JSON.stringify(content);
  }
  return fetch(URL + endpoint, init)
    .then(response => {
      if (response.status === 403) {
        // User disconnected
        throw response.status;
      }
      let parsedResponse = response.json();
      return parsedResponse.response || parsedResponse;
    })
    .then(value => {
      // console.log('Request response', value);
      if (!value.success) {
        throw value.success;
      }
      return value.response;
    })
    .catch(error => {
      if (dispatch) {
        console.log('Request authorization error', error);
        dispatch({type: 'SET_DISCONNECTED', value: true});
      }
      console.log('Request error', error);
      throw false;
    });
}

export default call;
