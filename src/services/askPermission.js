import {Platform} from 'react-native';
import {request, PERMISSIONS, RESULTS} from 'react-native-permissions';

async function requestPermission(permission) {
  return request(permission).then(result => {
    switch (result) {
      case RESULTS.UNAVAILABLE:
        console.log(
          'This feature is not available (on this device / in this context)',
        );
        break;
      case RESULTS.DENIED:
        console.log(
          'The permission has not been requested / is denied but requestable',
        );
        break;
      case RESULTS.LIMITED:
        console.log('The permission is limited: some actions are possible');
        break;
      case RESULTS.GRANTED:
        console.log('The permission is granted');
        return true;
      case RESULTS.BLOCKED:
        console.log('The permission is denied and not requestable anymore');
        break;
    }
    throw false;
  });
}

export async function askCamera() {
  let permission = PERMISSIONS.ANDROID.CAMERA;
  if (Platform.OS === 'ios') {
    permission = PERMISSIONS.IOS.CAMERA;
  }
  return requestPermission(permission);
}

export async function askGallery() {
  let permission = PERMISSIONS.ANDROID.CAMERA;
  if (Platform.OS === 'ios') {
    permission = PERMISSIONS.IOS.PHOTO_LIBRARY;
  }
  return requestPermission(permission);
}

export async function askContact() {
  let permission = PERMISSIONS.ANDROID.READ_CONTACTS;
  if (Platform.OS === 'ios') {
    permission = PERMISSIONS.IOS.CONTACTS;
  }
  return requestPermission(permission);
}

export async function askPosition(once = false) {
  let permission = PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
  if (Platform.OS === 'ios') {
    permission = PERMISSIONS.IOS.LOCATION_ALWAYS;
    if (once) {
      permission = PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
    }
  }
  return requestPermission(permission);
}
