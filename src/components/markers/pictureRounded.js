import React, {Component} from 'react';
import {Platform, Image, StyleSheet, Text, View} from 'react-native';
import User from '../../assets/icons/user-background.svg';
import {Colors} from '../../views/stylesVar';

class PictureRounded extends Component {
  render() {
    if (this.props.picture && this.props.picture.data) {
      return (
        <View style={styles.container}>
          <Text style={styles.textContainer}>
            <Image
              style={styles.userPicture}
              source={{
                uri: `data:${this.props.picture.mime};base64,${this.props.picture.data}`,
              }}
            />
          </Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <User style={styles.userPicture} height={34} width={34} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 38,
    height: 38,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: Colors.base,
    overflow: 'hidden',
  },
  textContainer: {
    height: '200%',
    marginTop: Platform.OS === 'ios' ? 3 : -13,
  },
  userPicture: {
    width: 34,
    height: 34,
  },
});

export default PictureRounded;
