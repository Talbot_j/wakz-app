import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {Callout} from 'react-native-maps';
import {connect} from 'react-redux';
import PictureRounded from './pictureRounded';
import {Colors, FontsSize} from '../../views/stylesVar';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
    currentPosition: state.currentPosition,
    conversations: state.conversations,
    user: state.user,
    contacts: state.contacts,
    groups: state.groups,
    unlockedList: state.unlockedList,
  };
};

export class MyCallout extends React.Component {
  state = {
    isSelected: false,
  };

  render() {
    return (
      <Callout tooltip={true} onPress={() => this.props.onClickOnCallout()}>
        <View style={styles.panel}>
          <View style={styles.pictureContainer}>
            <PictureRounded picture={this.props.picture} />
          </View>
          <View>
            <Text style={styles.groupText}>{this.props.title}</Text>
            <View>
              <Text style={styles.groupSubText}>
                {this.props.date} {this.props.distance.distance}{' '}
                {this.props.distance.unit}
              </Text>
            </View>
          </View>
        </View>
      </Callout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  groupText: {
    fontSize: FontsSize.base,
  },
  panel: {
    display: 'flex',
    flexDirection: 'row',
    width: 200,
    height: 60,
    backgroundColor: Colors.accent,
    borderRadius: 5,
    alignItems: 'center',
  },
  pictureContainer: {
    marginLeft: 10,
    marginRight: 10,
  },

  marker: {
    height: 20,
    width: 20,
  },
});

export default connect(mapStateToProps)(MyCallout);
