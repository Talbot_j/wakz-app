import React, {Component} from 'react';
import {
  TextInput,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {ArrayCharUnderlined} from './arrayCharUnderlined';

export class InputSMSCode extends Component {
  componentDidMount(): void {
    this.focusInput();
  }

  focusInput() {
    this.input.focus();
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => this.focusInput()}>
        <View style={styles.container}>
          <TextInput
            ref={input => {
              this.input = input;
            }}
            style={styles.hiddenInput}
            keyboardType={'numeric'}
            maxLength={this.props.size}
            value={this.props.value}
            onChangeText={phoneAuth => this.props.onSmsCode(phoneAuth)}
          />
          <ArrayCharUnderlined
            size={this.props.size}
            value={this.props.value}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '60%',
    marginLeft: '20%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  hiddenInput: {
    position: 'absolute',
    height: 0,
    width: 0,
    opacity: 0,
  },
});
