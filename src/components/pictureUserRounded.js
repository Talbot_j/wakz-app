import React, {Component} from 'react';
import {Image, StyleSheet} from 'react-native';
import {View} from 'react-native';
import User from '../assets/icons/user-background.svg';

class PictureUserRounded extends Component {
  render() {
    let image;
    if (this.props.picture && this.props.picture.data) {
      image = (
        <Image
          style={styles.userPicture}
          source={{
            uri: `data:${this.props.picture.mime};base64,${this.props.picture.data}`,
          }}
        />
      );
    } else {
      image = <User style={styles.userPicture} height={32} width={32} />;
    }

    return <View>{image}</View>;
  }
}

const styles = StyleSheet.create({
  userPicture: {
    height: 32,
    width: 32,
    borderRadius: 20,
  },
});

export default PictureUserRounded;
