import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../views/stylesVar';
import Check from '../assets/icons/check.svg';

class CheckBoxRounded extends Component {
  render() {
    let backgroundStyle = {};
    let checkIcon = null;
    if (this.props.isChecked) {
      checkIcon = <Check style={styles.checkIcon} />;
      backgroundStyle = {
        backgroundColor: Colors.accent,
      };
    }

    return <View style={[styles.container, backgroundStyle]}>{checkIcon}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    borderColor: Colors.accent,
    borderWidth: 2,
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  checkIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 'auto',
    marginBottom: 'auto',
    color: Colors.base,
  },
});

export default CheckBoxRounded;
