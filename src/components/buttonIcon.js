import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {Colors} from '../views/stylesVar';

class ButtonIcon extends Component {
  render() {
    if (this.props.hidden) {
      return null;
    }
    let color = this.props.color ? this.props.color : Colors.accent;
    let size = this.props.size ? this.props.size : '32';

    return (
      <TouchableOpacity onPress={() => this.props.callBack()}>
        <this.props.icon height={size} width={size} color={color} />
      </TouchableOpacity>
    );
  }
}

export default ButtonIcon;
