import {Animated} from 'react-native';

export default function shakeAnimate(animation) {
  Animated.sequence([
    Animated.timing(animation, {
      toValue: 10,
      duration: 50,
      useNativeDriver: true,
    }),
    Animated.timing(animation, {
      toValue: -10,
      duration: 50,
      useNativeDriver: true,
    }),
    Animated.timing(animation, {
      toValue: 10,
      duration: 50,
      useNativeDriver: true,
    }),
    Animated.timing(animation, {
      toValue: 0,
      duration: 50,
      useNativeDriver: true,
    }),
  ]).start();
}
