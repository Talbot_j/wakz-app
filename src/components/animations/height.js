import React, {useRef, useEffect} from 'react';
import {Animated} from 'react-native';

export const AnimationHeight = props => {
  const heightAnim = useRef(
    new Animated.Value(props.animation.height[0]),
  ).current; // Initial value for opacity: 0
  const fadeAnim = useRef(
    new Animated.Value(props.animation.opacity[0]),
  ).current; // Initial value for opacity: 0

  useEffect(() => {
    Animated.parallel([
      Animated.timing(fadeAnim, {
        toValue: props.animation.opacity[1],
        duration: 500,
        useNativeDriver: false,
      }),
      Animated.timing(heightAnim, {
        toValue: props.animation.height[1],
        duration: 600,
        useNativeDriver: false,
      }),
    ]).start(() => {
      // callback
    });
  }, [heightAnim, fadeAnim, props.animation.opacity, props.animation.height]);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        height: heightAnim, // Bind opacity to animated value
        opacity: fadeAnim,
      }}>
      {props.children}
    </Animated.View>
  );
};
