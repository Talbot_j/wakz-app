import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import Cross from '../assets/icons/cross-circled.svg';
import PictureUserRounded from './pictureUserRounded';
import {Colors} from '../views/stylesVar';

export class SelectorListUserRoundedDeletable extends Component {
  clickItem(item) {
    let selection = this.props.users;
    let indexOf = this.props.users.indexOf(item);
    if (indexOf !== -1) {
      selection.splice(indexOf, 1);
    }
    this.props.onDeleteUser(selection);
  }

  renderItem = ({item /*, index */}) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.clickItem(item)}>
        <Cross width={12} height={12} style={styles.crossIcon} />
        <PictureUserRounded picture={item.profilePicture || item.picture} />
        <Text numberOfLines={1} style={styles.itemText}>
          {item.firstName || item.title}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View>
        <FlatList
          horizontal={true}
          data={this.props.users}
          renderItem={item => this.renderItem(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    marginTop: 24,
    marginBottom: 0,
    marginRight: 0,
    marginLeft: 12,
    justifyContent: 'center',
    alignItems: 'center',
    width: 56,
    height: 60,
  },
  itemText: {
    color: Colors.accent,
  },
  crossIcon: {
    backgroundColor: Colors.base,
    color: Colors.accent,
    position: 'absolute',
    right: 0,
    top: 0,
    width: 6,
    height: 6,
  },
});

export default SelectorListUserRoundedDeletable;
