import React, {Component} from 'react';
import CharUnderlined from './charUnderlined';
import {StyleSheet, View} from 'react-native';

export class ArrayCharUnderlined extends Component {
  render() {
    let elements = [];
    for (let i = 0; i < this.props.size; i++) {
      elements.push(<CharUnderlined value={this.props.value[i]} key={i} />);
    }
    return <View style={style.container}>{elements}</View>;
  }
}
const style = StyleSheet.create({
  container: {
    marginTop: 12,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
