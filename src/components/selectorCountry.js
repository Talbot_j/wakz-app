import React, {Component} from 'react';
import {
  NativeModules,
  Platform,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import CountryPicker, {
  getAllCountries,
} from 'react-native-country-picker-modal';
import Caret from '../assets/icons/caret.svg';
import {LoginInput, Colors} from '../views/stylesVar';

class SelectorCountry extends Component {
  state = {
    forceVisible: false,
  };

  getDeviceCountry() {
    // noinspection JSUnresolvedVariable
    let deviceLanguage =
      Platform.OS === 'ios'
        ? NativeModules.SettingsManager.settings.AppleLocale ||
          NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
        : NativeModules.I18nManager.localeIdentifier;
    return deviceLanguage.replace(/[^A-Z]/g, '');
  }

  render() {
    const onSelectCountry = (country = null) => {
      this.setState({forceVisible: false});
      if (country) {
        this.props.onSelectCountry(country);
      }
    };

    if (this.props.countryCode === '') {
      let deviceCountry = this.getDeviceCountry();
      getAllCountries().then(countries => {
        const country = countries.find(c => c.cca2 === deviceCountry);
        this.props.onSelectCountry(country);
      });
    }

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          this.setState({forceVisible: true});
        }}>
        <CountryPicker
          {...{
            cca2: this.props.countryCode,
            countryCode: this.props.countryCode,
            withCallingCodeButton: false,
            withAlphaFilter: true,
            withCallingCode: true,
            withFilter: true,
            withEmoji: true,
            withFlag: true,
            withFlagButton: false,
            withCountryNameButton: true,
            onSelect: onSelectCountry,
            onClose: onSelectCountry,
          }}
          visible={this.state.forceVisible}
        />
        <Caret style={styles.caretIcon} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...LoginInput(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  caretIcon: {
    color: Colors.black,
    position: 'absolute',
    left: '90%',
  },
});

export default SelectorCountry;
