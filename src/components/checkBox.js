import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Colors} from '../views/stylesVar';
import Check from '../assets/icons/check.svg';

class CheckBox extends Component {
  render() {
    let checkIcon = this.props.isChecked ? (
      <Check style={styles.checkIcon} />
    ) : null;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.onValueChange()}>
        {checkIcon}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 6,
    width: 20,
    height: 20,
    borderColor: Colors.accent,
    borderWidth: 2,
  },
  checkIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 'auto',
    marginBottom: 'auto',
    color: Colors.accent,
  },
});

export default CheckBox;
