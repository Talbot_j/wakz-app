import React, {Component} from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {Colors} from '../views/stylesVar';

class ModalTwoAnswer extends Component {
  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isVisible}>
        <TouchableWithoutFeedback onPress={this.props.dismiss}>
          <View style={styles.modalOverlay} />
        </TouchableWithoutFeedback>
        <View style={styles.modalView}>
          {this.props.question}
          <TouchableHighlight
            onPress={() => {
              this.props.callBack(true);
            }}>
            <Text style={styles.textStyle}>{this.props.acceptText}</Text>
          </TouchableHighlight>
          <View style={styles.separator} />
          <TouchableHighlight
            onPress={() => {
              this.props.callBack(false);
            }}>
            <Text style={styles.textStyle}>{this.props.denyText}</Text>
          </TouchableHighlight>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: '100%',
  },
  modalView: {
    flex: 0.3,
    top: '37.5%',
    height: '25%',
    left: '10%',
    width: '80%',
    position: 'absolute',
    justifyContent: 'center',
    backgroundColor: Colors.white,
    padding: 15,
    alignItems: 'flex-start',
    textAlign: 'left',
    elevation: 5,
  },
  textStyle: {
    color: Colors.accent,
    fontWeight: 'bold',
    marginTop: 8,
    marginBottom: 8,
  },
  phoneNumberText: {
    marginTop: 16,
    marginBottom: 8,
    fontSize: 18,
  },
  modalOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});

export default ModalTwoAnswer;
