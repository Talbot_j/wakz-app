import React, {Component} from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Colors} from '../views/stylesVar';

class ButtonTextBackground extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[styles.button, this.props.disabled ? styles.disabled : null]}
        disabled={this.props.disabled}
        onPress={() => this.props.callBack()}>
        <Text style={styles.buttonText}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.accent,
    margin: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 3,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 7,
    alignItems: 'center',
  },
  disabled: {
    backgroundColor: Colors.accent + '66',
  },
  buttonText: {
    fontSize: 20,
  },
});

export default ButtonTextBackground;
