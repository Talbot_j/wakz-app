import React from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import Cross from '../assets/icons/cross-circled.svg';
import PictureUserRounded from './pictureUserRounded';
import {Colors, FontsSize} from '../views/stylesVar';

export default class SelectorListUserDeletable extends React.Component {
  clickItem = item => {
    let selection = this.props.users;
    let indexOf = this.props.users.indexOf(item);
    if (indexOf !== -1) {
      selection.splice(indexOf, 1);
      this.props.onDeleteUser(selection);
    }
  };

  renderItem = ({item /*, index */}) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => this.clickItem(item)}>
        <PictureUserRounded picture={item.profilePicture || item.picture} />
        <Text style={styles.itemText}>{item.firstName}</Text>
        <Cross style={styles.crossIcon} height={16} width={16} />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.sectionHeader}>
          {this.props.users.length} Members
        </Text>
        <FlatList
          data={this.props.users}
          renderItem={item => this.renderItem(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 32,
  },
  sectionHeader: {
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 24,
    paddingRight: 24,
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.base,
    backgroundColor: Colors.white,
  },
  itemContainer: {
    display: 'flex',
    paddingLeft: 24,
    paddingRight: 24,
    height: 52,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemText: {
    marginLeft: 24,
    marginRight: 'auto',
    fontSize: FontsSize.base,
    color: Colors.accent,
  },
  crossIcon: {
    color: Colors.accent,
  },
});
