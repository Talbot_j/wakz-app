import React, {Component} from 'react';
import {StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {View} from 'react-native';
import {Colors} from '../views/stylesVar';

class SelectorIcon extends Component {
  render() {
    let colorIcon = this.props.colorIcon ? this.props.colorIcon : Colors.accent;
    let colorIconSelected = this.props.colorIconSelected
      ? this.props.colorIconSelected
      : Colors.base;
    let colorBackground = this.props.colorBackground
      ? this.props.colorBackground
      : Colors.base;

    return (
      <TouchableWithoutFeedback onPress={() => this.props.onPress()}>
        <View
          style={[
            styles.iconContainer,
            this.props.selected
              ? {backgroundColor: colorIcon}
              : {backgroundColor: colorBackground},
          ]}>
          <this.props.icon
            color={this.props.selected ? colorIconSelected : colorIcon}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  iconContainer: {
    margin: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    height: 36,
    width: 36,
  },
});

export default SelectorIcon;
