import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View, Image} from 'react-native';
import User from '../assets/icons/user-background.svg';
import Cross from '../assets/icons/cross-circled.svg';
import ImagePicker from 'react-native-image-crop-picker';
import {askCamera, askGallery} from '../services/askPermission';
import {Colors} from '../views/stylesVar';
import ModalCameraOrGallery from './modalCameraOrGallery';

const options = {
  width: 300,
  height: 400,
  cropperCircleOverlay: true,
  showCropGuidelines: false,
  cropping: true,
  includeBase64: true,
};

class ImagePickerCropper extends Component {
  state = {
    confirmModalVisible: false,
  };
  showModal(value = true) {
    this.setState({
      confirmModalVisible: value,
    });
  }
  async onModalExit(result) {
    // ToDo: Find another workaround
    setTimeout(() => {
      if (result) {
        this.openPicker();
      } else {
        this.openGallery();
      }
    }, 550);
    this.showModal(false);
  }

  async openPicker() {
    askCamera().then(granted => {
      if (granted) {
        ImagePicker.openCamera(options).then(response => {
          this.props.callBack({data: response.data, mime: response.mime});
        });
      }
    });
  }
  async openGallery() {
    askGallery().then(granted => {
      if (granted) {
        ImagePicker.openPicker(options).then(response => {
          this.props.callBack({data: response.data, mime: response.mime});
        });
      }
    });
  }

  clearImage() {
    this.props.callBack({data: '', mime: ''});
  }

  render() {
    let image;
    let crossIcon;
    if (this.props.value) {
      image = (
        <Image
          style={styles.userPicture}
          source={{
            uri: `data:${this.props.value.mime};base64,${this.props.value.data}`,
          }}
        />
      );
      crossIcon = (
        <TouchableOpacity
          onPress={() => this.clearImage()}
          style={styles.crossIcon}>
          <Cross width={24} height={24} style={styles.crossIcon} />
        </TouchableOpacity>
      );
    } else {
      image = <User style={styles.userPicture} height={150} width={150} />;
    }

    return (
      <View style={styles.content}>
        {crossIcon}
        <TouchableOpacity onPress={() => this.showModal()}>
          {image}
        </TouchableOpacity>
        <ModalCameraOrGallery
          dismiss={() => this.showModal(false)}
          callBack={r => this.onModalExit(r)}
          isVisible={this.state.confirmModalVisible}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    margin: '8%',
    height: 150,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  userPicture: {
    borderRadius: 5000,
    height: 150,
    width: 150,
  },
  crossIcon: {
    zIndex: 50000,
    borderRadius: 25,
    position: 'absolute',
    right: 4,
    top: 4,
    width: 24,
    height: 24,
    color: Colors.accent,
  },
});

export default ImagePickerCropper;
