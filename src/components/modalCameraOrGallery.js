import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ModalTwoAnswer from './modalTwoAnswer';

class ModalCameraOrGallery extends Component {
  render() {
    const questionView = () => {
      return (
        <View style={styles.container}>
          <Text>How do you want to provide a picture?</Text>
        </View>
      );
    };

    return (
      <ModalTwoAnswer
        dismiss={this.props.dismiss}
        callBack={ret => this.props.callBack(ret)}
        isVisible={this.props.isVisible}
        question={questionView()}
        acceptText={'Camera'}
        denyText={'Gallery'}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 'auto',
  },
});

export default ModalCameraOrGallery;
