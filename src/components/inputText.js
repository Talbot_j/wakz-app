import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  TextInput,
} from 'react-native';
import {LoginInput} from '../views/stylesVar';

class InputText extends Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={() => this.inputRef.focus()}>
        <View style={styles.container}>
          <TextInput
            ref={ref => (this.inputRef = ref)}
            maxLength={this.props.length}
            placeholder={this.props.placeholder}
            underlineColorAndroid="transparent"
            onChangeText={value => this.props.callBack(value)}
            value={this.props.value}
            style={styles.transparentInput}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...LoginInput(),
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15,
  },
  transparentInput: {
    marginLeft: 20,
    padding: 0,
  },
});

export default InputText;
