import React, {Component} from 'react';
import {
  SectionList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import CheckBoxRounded from './checkBoxRounded';
import {connect} from 'react-redux';
import {Colors, FontsSize} from '../views/stylesVar';
import PictureUserRounded from './pictureUserRounded';
import {sectionFromContactsAndGroups} from '../services/sectionFactory';

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    groups: state.groups,
    user: state.user,
  };
};

export class SelectorListUser extends Component {
  clickItem(item) {
    let selection = this.props.selected;
    let indexOf = this.props.selected.indexOf(item);
    if (indexOf !== -1) {
      selection.splice(indexOf, 1);
    } else {
      selection.push(item);
    }
    this.props.onSelectUser(selection);
  }

  renderItem = ({item /*, index */}) => {
    return (
      <TouchableOpacity
        onPress={() => this.clickItem(item)}
        style={styles.itemContainer}>
        <PictureUserRounded picture={item.profilePicture || item.picture} />
        <Text style={styles.itemText}>
          {item.firstName || item.title || 'Unnamed group'}
        </Text>
        <CheckBoxRounded isChecked={this.props.selected.includes(item)} />
      </TouchableOpacity>
    );
  };
  renderHeader = ({section /*, index */}) => {
    return <Text style={styles.sectionHeader}>{section.title}</Text>;
  };

  render() {
    return (
      <View style={styles.container}>
        <SectionList
          sections={sectionFromContactsAndGroups(
            this.props.user,
            this.props.contacts,
            this.props.contactOnly ? null : this.props.groups,
          )}
          renderItem={item => this.renderItem(item)}
          renderSectionHeader={section => this.renderHeader(section)}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 22,
    height: '70%',
    width: '100%',
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  itemContainer: {
    display: 'flex',
    paddingLeft: 24,
    paddingRight: 24,
    height: 52,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemText: {
    marginLeft: 24,
    marginRight: 'auto',
    fontSize: FontsSize.base,
    color: Colors.accent,
  },
});

export default connect(mapStateToProps)(SelectorListUser);
