import React, {Component} from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native';
import {LoginInput} from '../views/stylesVar';

class InputPhone extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.callingCode}>
          <Text>+{this.props.countryCallingCode}</Text>
        </View>
        <TextInput
          style={styles.transparentInput}
          maxLength={this.props.length}
          keyboardType={'numeric'}
          underlineColorAndroid="transparent"
          placeholder={this.props.placeholder}
          onChangeText={this.props.onChangePhoneNumber}
          value={this.props.phoneNumber}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...LoginInput(),
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15,
  },
  callingCode: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  transparentInput: {
    borderLeftWidth: 1,
    flex: 0.8,
    paddingLeft: 20,
    padding: 0,
  },
});

export default InputPhone;
