import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors, FontsSize} from '../views/stylesVar';

import ButtonBack from './buttonBack';
import ButtonText from './buttonText';
import ButtonIcon from './buttonIcon';
import Logo from '../assets/markers/plain-accent.svg';

export class Header extends React.Component {
  render() {
    let nextButton = this.props.nextIcon ? (
      <ButtonIcon icon={Logo} callBack={() => this.props.callNext()} />
    ) : (
      <ButtonText
        title={this.props.nextTitle}
        callBack={() => this.props.callNext()}
      />
    );

    return (
      <View style={styles.container}>
        <ButtonBack
          callBack={() => this.props.callBack()}
          absolutePosition={false}
        />
        <Text style={styles.title}>{this.props.title}</Text>
        {nextButton}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    height: 76,
    backgroundColor: Colors.base,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 36,
    paddingRight: 36,
  },
  title: {
    color: Colors.accent,
    fontSize: FontsSize.h6,
    textAlignVertical: 'center',
  },
});

export default Header;
