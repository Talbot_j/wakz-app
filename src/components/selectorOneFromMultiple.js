import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableWithoutFeedback} from 'react-native';
import {Colors} from '../views/stylesVar';

class SelectorOneFromMultiple extends Component {
  render() {
    let choices = [];
    this.props.selection.forEach((s, index) => {
      let s_styles = [styles.selector];
      if (index === 0) {
        s_styles.push(styles.left);
      } else if (index === this.props.selection.length - 1) {
        s_styles.push(styles.right);
      }
      if (index === this.props.selected) {
        s_styles.push(styles.active);
      }
      choices.push(
        <TouchableWithoutFeedback
          key={index}
          onPress={() => this.props.onChangeSelected(index)}>
          <View style={s_styles}>
            <Text style={styles.text}>{s}</Text>
          </View>
        </TouchableWithoutFeedback>,
      );
    });
    return <View style={styles.container}>{choices}</View>;
  }
}
const radius = 6;
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: 18,
    marginRight: 18,
    height: 24,
  },
  left: {
    borderTopLeftRadius: radius,
    borderBottomLeftRadius: radius,
  },
  right: {
    borderTopRightRadius: radius,
    borderBottomRightRadius: radius,
  },
  active: {
    backgroundColor: Colors.accent,
  },
  selector: {
    flex: 1,
    backgroundColor: Colors.base,
    borderColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
  },
  text: {
    color: Colors.white,
  },
});

export default SelectorOneFromMultiple;
