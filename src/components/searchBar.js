import React, {Component} from 'react';
import {StyleSheet, TextInput} from 'react-native';
import {Colors} from '../views/stylesVar';
import {View} from 'react-native';
import Loop from '../assets/icons/loop.svg';

class SearchBar extends Component {
  callBack(name) {
    this.props.callBack(name);
  }

  render() {
    return (
      <View style={styles.container}>
        <Loop style={styles.icon} />
        <TextInput
          maxLength={24}
          placeholder="Search"
          underlineColorAndroid="transparent"
          placeholderTextColor={Colors.accent}
          onChangeText={name => this.callBack(name)}
          style={styles.transparentInput}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: '5%',
    marginRight: '5%',
    backgroundColor: Colors.white,
    borderColor: Colors.dark,
    borderRadius: 20,
    flexDirection: 'row',
  },
  icon: {
    marginLeft: 16,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  transparentInput: {
    marginLeft: 16,
    padding: 0,
    color: Colors.accent,
  },
});

export default SearchBar;
