import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors, FontsSize} from '../views/stylesVar';

export default class CharUnderlined extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {this.props.value ? this.props.value : '  '}
        </Text>
        <View style={styles.dashedLine} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  text: {
    fontSize: FontsSize.h5,
    color: Colors.white,
  },
  dashedLine: {
    marginTop: 6,
    backgroundColor: Colors.white,
    width: '65%',
    height: 2,
  },
});
