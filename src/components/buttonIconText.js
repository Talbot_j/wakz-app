import React, {Component} from 'react';
import {StyleSheet, TouchableWithoutFeedback, View, Text} from 'react-native';
import {Colors, FontsSize} from '../views/stylesVar';

class ButtonIconText extends Component {
  render() {
    let size = this.props.size ? this.props.size : 48;

    return (
      <TouchableWithoutFeedback onPress={() => this.props.onPress()}>
        <View style={styles.container}>
          <this.props.icon style={styles.icon} width={size} height={size} />
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.base,
    borderRadius: 6,
    width: '90%',
    height: 72,
    marginTop: 24,
  },
  icon: {
    color: Colors.accent,
    marginLeft: 12,
    marginRight: 24,
  },
  title: {
    color: Colors.white,
    fontSize: FontsSize.h4,
  },
});

export default ButtonIconText;
