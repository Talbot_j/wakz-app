import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import {Colors, FontsSize} from '../views/stylesVar';

class ButtonText extends Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={() => this.props.callBack()}>
        <Text style={styles.text}>
          {this.props.title ? this.props.title : 'Click'}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    textAlignVertical: 'center',
  },
  text: {
    color: Colors.accent,
    fontSize: FontsSize.base,
  },
});

export default ButtonText;
