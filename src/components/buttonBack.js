import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

import {Colors} from '../views/stylesVar';
import Caret from '../assets/icons/caret.svg';

class ButtonBack extends Component {
  render() {
    let size = this.props.size ? this.props.size : 24;
    let color = this.props.color ? this.props.color : Colors.accent;

    return (
      <TouchableOpacity
        style={this.props.absolutePosition ? styles.containerAbsolute : null}
        onPress={() => this.props.callBack()}>
        <Caret style={styles.left} width={size} height={size} color={color} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  left: {
    transform: [{rotateY: '180deg'}],
  },
  container: {},
  containerAbsolute: {
    position: 'absolute',
    zIndex: 100,
    left: 36,
    top: 36,
  },
});

export default ButtonBack;
