import React from 'react';
import {StyleSheet, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import * as TransitionPresets from '@react-navigation/stack/lib/commonjs/TransitionConfigs/TransitionPresets';

import MapWax from './content/map';
import ListWax from './content/list';
import Settings from './settings/settings';
import NewWax from './newWax/newWax';
import NewGroup from './newGroup/newGroup';
import Conversation from './conversation/conversation';
import HeaderMenu from './header/headerMenu';

import FilterPanel from './content/components/filterPanel';
import OverlayButtonNew from './content/components/overlayButtonNew';

function MapScreen({navigation}) {
  return <MapWax navigation={navigation} />;
}
function ListScreen({navigation}) {
  return <ListWax navigation={navigation} />;
}
function SettingsScreen({navigation}) {
  return <Settings navigation={navigation} />;
}
function NewWaxScreen({navigation}) {
  return <NewWax navigation={navigation} />;
}
function NewGroupScreen({navigation}) {
  return <NewGroup navigation={navigation} />;
}
function ConversationScreen({navigation}) {
  return <Conversation navigation={navigation} />;
}

const AppStack = createStackNavigator();
const MainViewStack = createStackNavigator();

export class Home extends React.Component {
  MainViewScreen({navigation}) {
    return (
      <View style={styles.container}>
        <HeaderMenu navigation={navigation} />
        <MainViewStack.Navigator
          initialRouteName="Map"
          screenOptions={{...TransitionPresets.SlideFromRightIOS}}>
          <MainViewStack.Screen
            name="List"
            component={ListScreen}
            options={{headerShown: false}}
          />
          <MainViewStack.Screen
            name="Map"
            component={MapScreen}
            options={{headerShown: false}}
          />
          <MainViewStack.Screen
            name="Settings"
            component={SettingsScreen}
            options={{headerShown: false}}
          />
        </MainViewStack.Navigator>
        <FilterPanel />
        <OverlayButtonNew navigation={navigation} />
      </View>
    );
  }

  render() {
    return (
      <NavigationContainer>
        <AppStack.Navigator mode="modal" initialRouteName="MainView">
          <AppStack.Screen
            name="MainView"
            component={this.MainViewScreen}
            options={{headerShown: false}}
          />
          <AppStack.Screen
            name="NewWax"
            component={NewWaxScreen}
            options={{headerShown: false}}
          />
          <AppStack.Screen
            name="NewGroup"
            component={NewGroupScreen}
            options={{headerShown: false}}
          />
          <AppStack.Screen
            name="Conversation"
            component={ConversationScreen}
            options={{headerShown: false}}
          />
        </AppStack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Home;
