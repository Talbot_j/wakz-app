import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors, FontCentered, FontsSize} from '../../stylesVar';

import ModalPhoneCheck from '../components/modalPhoneCheck';
import SelectorCountry from '../../../components/selectorCountry';

import {isPossiblePhoneNumber} from 'react-phone-number-input';
import {postPhone} from '../../../services/apiCalls/auth';
import ButtonTextBackground from '../../../components/buttonTextBackground';
import InputPhone from '../../../components/inputPhone';

class StepTwo extends Component {
  state = {
    confirmModalVisible: false,
  };

  render() {
    const isPossible = (
      countryCC = this.props.phoneInfo.countryCallingCode,
      phoneNumber = this.props.phoneInfo.phoneNumber,
    ) => {
      return isPossiblePhoneNumber('+' + countryCC + phoneNumber);
    };

    const onChangeCountry = country => {
      if (!country) {
        return;
      }
      this.props.onPhoneChange({
        isPossible: isPossible(country.callingCode),
        countryCallingCode: country.callingCode,
        countryCode: country.cca2,
        phoneNumber: this.props.phoneInfo.phoneNumber,
      });
    };
    const onChangePhoneNumber = phoneNumber => {
      this.props.onPhoneChange({
        isPossible: isPossible(undefined, phoneNumber),
        phoneNumber: phoneNumber,
        countryCallingCode: this.props.phoneInfo.countryCallingCode,
        countryCode: this.props.phoneInfo.countryCode,
      });
    };
    const onModalExit = result => {
      this.setState({confirmModalVisible: false});
      if (result) {
        postPhone(
          '+' +
            this.props.phoneInfo.countryCallingCode +
            this.props.phoneInfo.phoneNumber,
        ).then(
          () => {
            this.props.nextStep();
          },
          () => {
            this.console.log('todo: warn error on postPhone');
          },
        );
      }
    };

    return (
      <View>
        <Text style={styles.text}>Enter your phone number</Text>
        <SelectorCountry
          onSelectCountry={onChangeCountry}
          countryCode={this.props.phoneInfo.countryCode}
        />
        <InputPhone
          countryCallingCode={this.props.phoneInfo.countryCallingCode}
          phoneNumber={this.props.phoneInfo.phoneNumber}
          onChangePhoneNumber={onChangePhoneNumber}
          length={12}
          placeholder={'Phone Number'}
        />
        <ButtonTextBackground
          text="Send SMS"
          disabled={!this.props.phoneInfo.isPossible}
          callBack={() => this.setState({confirmModalVisible: true})}
        />
        <Text style={styles.tinyText}>
          The number should be able to receive SMS, so we can send you the code.
        </Text>
        <ModalPhoneCheck
          dismiss={() => onModalExit(false)}
          callBack={onModalExit}
          isVisible={this.state.confirmModalVisible}
          phoneNumber={
            '+' +
            this.props.phoneInfo.countryCallingCode +
            this.props.phoneInfo.phoneNumber
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    ...FontCentered(),
    color: Colors.white,
    fontSize: FontsSize.base,
    marginBottom: 8,
  },
  tinyText: {
    ...FontCentered(25),
    color: Colors.white,
    fontSize: FontsSize.tiny,
  },
});

export default StepTwo;
