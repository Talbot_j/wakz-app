import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors, FontCentered, FontsSize} from '../../stylesVar';

import AcceptPolicy from '../components/acceptPolicy';
import ButtonTextBackground from '../../../components/buttonTextBackground';
import ModalPolicy from '../components/modalPolicy';

class StepOne extends Component {
  state = {
    isAccepted: false,
    policyVisible: false,
  };

  render() {
    const setPolicyVisible = policyVisible => {
      this.setState({policyVisible: policyVisible});
    };
    const setIsAccepted = () => {
      this.setState({isAccepted: !this.state.isAccepted});
    };

    return (
      <View>
        <Text style={styles.titleText}>Welcome To Wax</Text>
        <Text style={styles.subtitleText}>
          Location based messaging application
        </Text>
        <AcceptPolicy
          isAccepted={this.state.isAccepted}
          setIsAccepted={setIsAccepted}
          setPolicyVisible={() => setPolicyVisible(true)}
        />
        <ButtonTextBackground
          text="Get Started"
          callBack={() => this.props.nextStep()}
          disabled={!this.state.isAccepted}
        />
        <ModalPolicy
          isVisible={this.state.policyVisible}
          onClose={() => setPolicyVisible(false)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleText: {
    ...FontCentered(),
    color: Colors.white,
    fontSize: FontsSize.h3,
  },
  subtitleText: {
    ...FontCentered(),
    color: Colors.white,
    fontSize: FontsSize.base,
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '60%',
  },
});

export default StepOne;
