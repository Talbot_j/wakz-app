import React, {Component} from 'react';
import {Animated, StyleSheet, Text, View} from 'react-native';
import ButtonTextBackground from '../../../components/buttonTextBackground';
import {Colors, FontCentered, FontsSize} from '../../stylesVar';
import {postPhone, postSmsCode} from '../../../services/apiCalls/auth';
import {connect} from 'react-redux';
import {InputSMSCode} from '../../../components/inputSMSCode';
import shakeAnimate from '../../../components/animations/shake';

class StepThree extends Component {
  shakeAnimation = new Animated.Value(0);
  state = {
    phoneAuth: '',
    timer: 70,
  };

  componentDidMount() {
    this._interval = setInterval(() => {
      if (this.state.timer > 0) {
        this.setState({timer: this.state.timer - 1});
      }
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this._interval);
  }

  resendSMS() {
    postPhone(this.props.phoneNumber).then(
      () => {
        this.setState({timer: 70});
      },
      () => {
        this.console.log('todo: warn error on postPhone');
      },
    );
  }

  checkCode(code) {
    postSmsCode(this.props.phoneNumber, code).then(
      success => {
        this.props.dispatch({type: 'SET_USER', value: success.user});
        this.props.dispatch({
          type: 'SET_AUTHORIZATION',
          value: success.authorization,
        });
      },
      () => {
        shakeAnimate(this.shakeAnimation);
        this.setState({phoneAuth: ''});
      },
    );
  }

  onCodeChange(code) {
    code = code.replace(/[^0-9]/g, '');
    this.setState({phoneAuth: code});
    if (code.length !== 6) {
      return;
    }
    this.checkCode(code);
  }

  render() {
    return (
      <View style={styles.content}>
        <Text style={styles.text}>
          We have sent you a code via SMS to phone number
          <Text style={styles.textBold}> {this.props.phoneNumber}. </Text>
          <Text onPress={this.props.previousStep} style={styles.textUnderline}>
            Wrong phone number?
          </Text>
        </Text>
        <View style={styles.margin} />
        <Text style={styles.title}>Your 6-digit code</Text>
        <Animated.View style={{transform: [{translateX: this.shakeAnimation}]}}>
          <InputSMSCode
            size={6}
            onSmsCode={code => this.onCodeChange(code)}
            value={this.state.phoneAuth}
          />
        </Animated.View>
        <View style={styles.margin} />
        <ButtonTextBackground
          text="Resend"
          disabled={this.state.timer > 0}
          callBack={() => this.resendSMS()}
        />
        <Text style={styles.text}>
          You got {this.state.timer} seconds to enter the code
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    ...FontCentered(),
    color: Colors.white,
    fontSize: FontsSize.little,
    marginBottom: 8,
  },
  textUnderline: {
    textDecorationLine: 'underline',
  },
  textBold: {
    fontWeight: 'bold',
  },
  margin: {
    height: 18,
  },
  title: {
    textAlign: 'center',
    color: Colors.white,
    fontSize: 20,
    letterSpacing: 4,
  },
});

export default connect()(StepThree);
