import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../stylesVar';

import Logo from '../../assets/logos/logo.svg';
import ButtonBack from '../../components/buttonBack';
import StepOne from './partials/stepOne';
import StepTwo from './partials/stepTwo';
import StepThree from './partials/stepThree';
import {connect} from 'react-redux';

export class Auth extends React.Component {
  state = {
    phoneInfos: {
      isPossible: false,
      phoneNumber: '',
      countryCode: '',
      countryCallingCode: '',
    },
    step: 1,
  };

  setStep(step) {
    this.setState({
      step: step,
    });
  }

  // Step Two
  onPhoneChange(phoneInfos) {
    this.setState({
      phoneInfos: phoneInfos,
    });
  }

  render() {
    let view = {
      logo: null,
      content: null,
      backButton: null,
    };
    switch (this.state.step) {
      case 1:
        view.logo = <Logo style={styles.logo} />;
        view.content = <StepOne nextStep={() => this.setStep(2)} />;
        break;
      case 2:
        view.logo = <Logo style={styles.logo} />;
        view.backButton = (
          <ButtonBack
            absolutePosition={true}
            callBack={() => this.setStep(1)}
          />
        );
        view.content = (
          <StepTwo
            nextStep={() => this.setStep(3)}
            phoneInfo={this.state.phoneInfos}
            onPhoneChange={phone => this.onPhoneChange(phone)}
          />
        );
        break;
      case 3:
        view.logo = <Logo style={styles.logo} />;
        view.backButton = (
          <ButtonBack
            absolutePosition={true}
            callBack={() => this.setStep(2)}
          />
        );
        view.content = (
          <StepThree
            phoneNumber={
              '+' +
              this.state.phoneInfos.countryCallingCode +
              this.state.phoneInfos.phoneNumber
            }
            previousStep={() => this.setStep(2)}
          />
        );
        break;
      default:
        break;
    }
    return (
      <View style={styles.container}>
        {view.backButton}
        {view.logo}
        <View style={styles.content}>{view.content}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.base,
    alignItems: 'center',
  },
  logo: {
    height: 120,
    width: '100%',
    margin: '8%',
  },
  content: {
    width: '100%',
    height: '85%',
  },
});

export default connect()(Auth);
