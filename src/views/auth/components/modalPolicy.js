import {Modal, StyleSheet, Text, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import {Colors, FontsSize} from '../../stylesVar';

class ModalPolicy extends Component {
  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        style={styles.modal}
        visible={this.props.isVisible}>
        <View style={styles.header}>
          <Text style={styles.closeButton} onPress={() => this.props.onClose()}>
            Close
          </Text>
          <Text style={styles.titleText}>Wax Privacy Policy</Text>
        </View>
        <ScrollView style={styles.content}>
          <Text style={styles.policyText}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            iaculis tincidunt gravida. Morbi semper posuere ipsum. Nulla non
            porta dui. Nullam eget elit vel sem facilisis rhoncus sit amet in
            nisi. Proin at aliquam sapien, quis efficitur purus. Nullam feugiat
            a odio sed molestie. Mauris ultricies enim felis, nec finibus purus
            tincidunt a. Vivamus a lectus pharetra, aliquam enim vitae, lacinia
            felis. In sit amet venenatis massa. Donec ac nunc a tortor finibus
            tristique vitae vitae odio. Cras mollis vehicula ligula, sit amet
            blandit enim congue quis. Interdum et malesuada fames ac ante ipsum
            primis in faucibus. Morbi vitae suscipit nibh. Donec sapien ipsum,
            aliquet sit amet dolor eget, commodo ornare lorem. Integer
            ullamcorper in sem vel faucibus. Donec et urna laoreet, euismod
            lorem in, dapibus nibh. Sed ut tortor est. Aenean vitae ante elit.
            Nunc sed quam justo. Suspendisse at orci tellus. Donec tempus porta
            sapien, id porttitor metus. Sed porttitor finibus ipsum, sit amet
            posuere urna ullamcorper vitae. Donec tempus, leo sit ltrices. Lorem
            ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            iaculis tincidunt gravida. Morbi semper posuere ipsum. Nulla non
            porta dui. Nullam eget elit vel sem facilisis rhoncus sit amet in
            nisi. Proin at aliquam sapien, quis efficitur purus. Nullam feugiat
            a odio sed molestie. Mauris ultricies enim felis, nec finibus purus
            tincidunt a. Vivamus a lectus pharetra, aliquam enim vitae, lacinia
            felis. In sit amet venenatis massa. Donec ac nunc a tortor finibus
            tristique vitae vitae odio. Cras mollis vehicula ligula, sit amet
            blandit enim congue quis. Interdum et malesuada fames ac ante ipsum
            primis in faucibus. Morbi vitae suscipit nibh. Donec sapien ipsum,
            aliquet sit amet dolor eget, commodo ornare lorem. Integer
            ullamcorper in sem vel faucibus. Donec et urna laoreet, euismod
            lorem in, dapibus nibh. Sed ut tortor est. Aenean vitae ante elit.
            Nunc sed quam justo. Suspendisse at orci tellus. Donec tempus porta
            sapien, id porttitor metus. Sed porttitor finibus ipsum, sit amet
            posuere urna ullamcorper vitae. Donec tempus, leo sit ltrices. Lorem
            ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            iaculis tincidunt gravida. Morbi semper posuere ipsum. Nulla non
            porta dui. Nullam eget elit vel sem facilisis rhoncus sit amet in
            nisi. Proin at aliquam sapien, quis efficitur purus. Nullam feugiat
            a odio sed molestie. Mauris ultricies enim felis, nec finibus purus
            tincidunt a. Vivamus a lectus pharetra, aliquam enim vitae, lacinia
            felis. In sit amet venenatis massa. Donec ac nunc a tortor finibus
            tristique vitae vitae odio. Cras mollis vehicula ligula, sit amet
            blandit enim congue quis. Interdum et malesuada fames ac ante ipsum
            primis in faucibus. Morbi vitae suscipit nibh. Donec sapien ipsum,
            aliquet sit amet dolor eget, commodo ornare lorem. Integer
            ullamcorper in sem vel faucibus. Donec et urna laoreet, euismod
            lorem in, dapibus nibh. Sed ut tortor est. Aenean vitae ante elit.
            Nunc sed quam justo. Suspendisse at orci tellus. Donec tempus porta
            sapien, id porttitor metus. Sed porttitor finibus ipsum, sit amet
            posuere urna ullamcorper vitae. Donec tempus, leo sit ltrices. Lorem
            ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            iaculis tincidunt gravida. Morbi semper posuere ipsum. Nulla non
            porta dui. Nullam eget elit vel sem facilisis rhoncus sit amet in
            nisi. Proin at aliquam sapien, quis efficitur purus. Nullam feugiat
            a odio sed molestie. Mauris ultricies enim felis, nec finibus purus
            tincidunt a. Vivamus a lectus pharetra, aliquam enim vitae, lacinia
            felis. In sit amet venenatis massa. Donec ac nunc a tortor finibus
            tristique vitae vitae odio. Cras mollis vehicula ligula, sit amet
            blandit enim congue quis. Interdum et malesuada fames ac ante ipsum
            primis in faucibus. Morbi vitae suscipit nibh. Donec sapien ipsum,
            aliquet sit amet dolor eget, commodo ornare lorem. Integer
            ullamcorper in sem vel faucibus. Donec et urna laoreet, euismod
            lorem in, dapibus nibh. Sed ut tortor est. Aenean vitae ante elit.
            Nunc sed quam justo. Suspendisse at orci tellus. Donec tempus porta
            sapien, id porttitor metus. Sed porttitor finibus ipsum, sit amet
            posuere urna ullamcorper vitae. Donec tempus, leo sit ltrices. Lorem
            ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            iaculis tincidunt gravida. Morbi semper posuere ipsum. Nulla non
            porta dui. Nullam eget elit vel sem facilisis rhoncus sit amet in
            nisi. Proin at aliquam sapien, quis efficitur purus. Nullam feugiat
            a odio sed molestie. Mauris ultricies enim felis, nec finibus purus
            tincidunt a. Vivamus a lectus pharetra, aliquam enim vitae, lacinia
            felis. In sit amet venenatis massa. Donec ac nunc a tortor finibus
            tristique vitae vitae odio. Cras mollis vehicula ligula, sit amet
            blandit enim congue quis. Interdum et malesuada fames ac ante ipsum
            primis in faucibus. Morbi vitae suscipit nibh. Donec sapien ipsum,
            aliquet sit amet dolor eget, commodo ornare lorem. Integer
            ullamcorper in sem vel faucibus. Donec et urna laoreet, euismod
            lorem in, dapibus nibh. Sed ut tortor est. Aenean vitae ante elit.
            Nunc sed quam justo. Suspendisse at orci tellus. Donec tempus porta
            sapien, id porttitor metus. Sed porttitor finibus ipsum, sit amet
            posuere urna ullamcorper vitae. Donec tempus, leo sit ltrices.
          </Text>
        </ScrollView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.base,
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    height: '10%',
    borderBottomColor: Colors.white,
    borderBottomWidth: 2,
  },
  content: {
    backgroundColor: Colors.base,
    height: '90%',
  },

  closeButton: {
    flex: 0.3,
    marginLeft: 24,
    color: Colors.white,
  },
  titleText: {
    flex: 0.7,
    marginRight: 24,
    color: Colors.white,
    fontSize: FontsSize.base,
  },

  policyText: {
    margin: 24,
    color: Colors.white,
  },
});

export default ModalPolicy;
