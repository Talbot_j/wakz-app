import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors, FontCentered, FontsSize} from '../../stylesVar';

import CheckBox from '../../../components/checkBox';

class AcceptPolicy extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          I understood and accept{' '}
          <Text
            onPress={this.props.setPolicyVisible}
            style={styles.textUnderline}>
            terms and conditions
          </Text>
        </Text>
        <CheckBox
          isChecked={this.props.isAccepted}
          onValueChange={this.props.setIsAccepted}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '60%',
    display: 'flex',
    flexDirection: 'row',
    marginTop: '10%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  text: {
    ...FontCentered(),
    color: Colors.white,
    fontSize: FontsSize.tiny,
    marginRight: 8,
    flex: 0.8,
  },
  textUnderline: {
    textDecorationLine: 'underline',
  },
});

export default AcceptPolicy;
