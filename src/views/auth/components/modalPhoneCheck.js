import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ModalTwoAnswer from '../../../components/modalTwoAnswer';

class ModalPhoneCheck extends Component {
  render() {
    const questionView = () => {
      return (
        <View>
          <Text>Is this your phone number?</Text>
          <Text style={styles.phoneNumberText}>{this.props.phoneNumber}</Text>
        </View>
      );
    };

    return (
      <ModalTwoAnswer
        dismiss={this.props.dismiss}
        callBack={ret => this.props.callBack(ret)}
        isVisible={this.props.isVisible}
        question={questionView()}
        acceptText={'Yes'}
        denyText={'Edit'}
      />
    );
  }
}

const styles = StyleSheet.create({
  phoneNumberText: {
    marginTop: 16,
    marginBottom: 8,
    fontSize: 18,
  },
});

export default ModalPhoneCheck;
