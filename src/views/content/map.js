import React from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import MapView from 'react-native-map-clustering';
import {PROVIDER_GOOGLE} from 'react-native-maps';
import mapStyle from '../../maps/default';
import {askPosition} from '../../services/askPermission';
import Geolocation from '@react-native-community/geolocation';
import {connect} from 'react-redux';
import {Colors} from '../stylesVar';
import MyMarker from './components/marker';
import MyCluster from './components/cluster';

const mapStateToProps = state => {
  return {
    currentPosition: state.currentPosition,
    authorization: state.authorization,
    conversations: state.conversations,
    user: state.user,
  };
};

const DIMENSIONS = Dimensions.get('window');
const ASPECT_RATIO = DIMENSIONS.width / DIMENSIONS.height;

export class Map extends React.Component {
  state = {
    focusedLocation: {
      latitude: 36.14,
      longitude: -86.76,
      latitudeDelta: 0.5,
      longitudeDelta: 0.5 * ASPECT_RATIO,
    },
    followUser: true,
    activeMarker: null,
  };

  componentDidMount(): void {
    this.props.dispatch({
      type: 'SET_IS_MAP_VIEW_VISIBLE',
      value: true,
    });
    this.props.dispatch({
      type: 'SET_IS_SETTINGS_VIEW_VISIBLE',
      value: false,
    });
    askPosition().then(
      () => {
        this.watchLocation().then(() => this.recenter());
      },
      () => {
        askPosition(true).then(
          () => {
            this.watchLocation().then(() => this.recenter());
          },
          () => {
            console.log('ToDo: Warn permission location denied');
          },
        );
      },
    );
  }

  async watchLocation() {
    return Geolocation.watchPosition(
      position => {
        const myPosition = position.coords;
        if (myPosition) {
          this.props.dispatch({
            type: 'SET_CURRENT_POSITION',
            value: myPosition,
          });
          if (this.state.followUser) {
            this.recenter();
          }
          return true;
        }
      },
      () => {
        return false;
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0,
        distanceFilter: 1,
      },
    );
  }

  mapView = React.createRef();
  superCluster = React.createRef();

  recenter(position = this.props.currentPosition) {
    if (this.mapView.current) {
      let newPos = {
        latitude: position.latitude
          ? position.latitude
          : this.state.focusedLocation.latitude,
        longitude: position.longitude
          ? position.longitude
          : this.state.focusedLocation.longitude,
        latitudeDelta: position.latitudeDelta
          ? position.latitudeDelta
          : this.state.focusedLocation.latitudeDelta,
        longitudeDelta: position.longitudeDelta
          ? position.longitudeDelta
          : this.state.focusedLocation.longitudeDelta,
      };
      this.setState({focusedLocation: newPos});
      this.mapView.current.animateToRegion(newPos, 100);
    }
  }

  toggleFollowUser() {
    if (!this.state.followUser) {
      this.recenter();
    }
    this.setState({followUser: !this.state.followUser});
  }

  onPressZoom(zoomIn = true) {
    let calc = zoomIn
      ? this.state.focusedLocation.latitudeDelta / 5
      : this.state.focusedLocation.latitudeDelta * 5;
    let latitudeDelta = calc < 0.0005 ? 0.00005 : calc;
    let newState = {
      latitude: this.state.focusedLocation.latitude,
      longitude: this.state.focusedLocation.longitude,
      latitudeDelta: latitudeDelta,
      longitudeDelta: latitudeDelta * ASPECT_RATIO,
    };
    this.setState({focusedLocation: newState});
    this.mapView.current.animateToRegion(newState, 1000);
  }

  redirectTo(conversation) {
    this.props.dispatch({
      type: 'SET_SELECTED_CONVERSATION',
      value: conversation,
    });
    this.props.navigation.push('Conversation');
  }
  onMarkerPress(markerId) {
    this.setState({activeMarker: markerId});
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          ref={this.mapView}
          superClusterRef={this.superCluster}
          renderCluster={cluster => {
            const {id, geometry, onPress} = cluster;
            // noinspection JSUnresolvedFunction
            const markers = this.superCluster.current.getLeaves(
              cluster.id,
              Infinity,
            );
            return (
              <MyCluster
                geometry={geometry}
                onPress={onPress}
                id={id}
                markers={markers}
                key={`cluster-${id}`}
              />
            );
          }}
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          customMapStyle={mapStyle}
          showsUserLocation={true}
          showsMyLocationButton={false}
          showsCompass={true}
          loadingEnabled={true}
          zoomEnabled={true}
          rotateEnabled={true}
          initialRegion={this.state.focusedLocation}
          onPress={() => this.onMarkerPress('-1')}
          onRegionChangeComplete={region => {
            this.setState({focusedLocation: region});
          }}>
          {this.props.conversations.map((item, index) => {
            return (
              <MyMarker
                onSelectMarker={id => this.onMarkerPress(id)}
                activeMarker={this.state.activeMarker}
                key={index}
                data={item}
                coordinate={{
                  latitude: item.latitude,
                  longitude: item.longitude,
                }}
                redirectTo={conversation => this.redirectTo(conversation)}
              />
            );
          })}
        </MapView>
        <View style={styles.buttons}>
          <TouchableOpacity
            style={styles.zoomButton}
            onPress={() => {
              this.onPressZoom(true);
            }}>
            <Text>+</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.zoomButton}
            onPress={() => {
              this.onPressZoom(false);
            }}>
            <Text>-</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.zoomButton}
            onPress={() => this.toggleFollowUser()}>
            <Text>{this.state.followUser ? 'U' : 'NU'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  zoomButton: {
    height: 32,
    width: 32,
    backgroundColor: Colors.light,
    alignItems: 'center',
    justifyContent: 'center',
  },
  marker: {
    height: 24,
    width: 24,
  },
  cluster: {
    height: 36,
    width: 36,
  },
  container: {
    flex: 1,
  },
  buttons: {
    width: 32,
    marginTop: 'auto',
  },
  newWaxMessage: {
    position: 'absolute',
    bottom: 12,
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default connect(mapStateToProps)(Map);
