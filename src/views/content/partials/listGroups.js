import React from 'react';
import {connect} from 'react-redux';
import Accordion from 'react-native-collapsible/Accordion';
import GroupRow from './groupRow';
import GroupContent from './groupContent';
import {ScrollView} from 'react-native';

const mapStateToProps = state => {
  return {
    groups: state.groups,
  };
};

export class ListGroups extends React.Component {
  state = {
    activeSections: [],
  };

  clickOnConversation = conversation => {
    this.props.dispatch({
      type: 'SET_SELECTED_CONVERSATION',
      value: conversation,
    });
    this.props.navigation.push('Conversation');
  };

  _updateSections = activeSections => {
    if (this.state.activeSections.includes(activeSections)) {
      this.setState({activeSections: []});
    } else {
      this.setState({activeSections: [activeSections]});
    }
  };

  _renderGroupRow = (section, index) => {
    let isHidden = !!(
      this.state.activeSections.length &&
      !this.state.activeSections.includes(index)
    );
    return (
      <GroupRow
        data={section}
        index={index}
        isHidden={isHidden}
        activeSections={this.state.activeSections}
        _updateSections={i => this._updateSections(i)}
      />
    );
  };

  _renderContent = section => {
    return (
      <GroupContent
        data={section}
        clickOnConversation={c => this.clickOnConversation(c)}
      />
    );
  };

  render() {
    return (
      <ScrollView>
        <Accordion
          duration={100}
          sections={this.props.groups}
          activeSections={this.state.activeSections}
          renderHeader={this._renderGroupRow}
          renderContent={this._renderContent}
          onChange={this._updateSections}
        />
      </ScrollView>
    );
  }
}

export default connect(mapStateToProps)(ListGroups);
