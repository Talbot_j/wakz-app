import React from 'react';
import {StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import {Colors} from '../../stylesVar';
import {SwipeRow} from 'react-native-swipe-list-view';
import Caret from '../../../assets/icons/caret.svg';
import {AnimationHeight} from '../../../components/animations/height';
import {connect} from 'react-redux';
import BackRow from '../components/backRow';
import calculateDistance from '../../../services/distanceCalculator';
import PictureUserRounded from '../../../components/pictureUserRounded';

const mapStateToProps = state => {
  return {
    user: state.user,
    contacts: state.contacts,
    currentPosition: state.currentPosition,
  };
};

export class GroupRow extends React.Component {
  render() {
    let user = {firstName: '', profilePicture: null};
    let dist = calculateDistance(this.props.data, this.props.currentPosition);
    if (!this.props.data.title) {
      let user_id =
        this.props.data.users.find(contact => contact !== this.props.user.id) ||
        this.props.user.id;
      user = this.props.contacts.find(contact => contact.id === user_id);
    }
    let row = (
      <SwipeRow leftOpenValue={150} rightOpenValue={-75}>
        <BackRow />
        <TouchableWithoutFeedback
          onPress={() => this.props._updateSections(this.props.index)}>
          <View style={styles.groupLine}>
            <View style={styles.groupPicture}>
              <PictureUserRounded
                picture={this.props.data.profilePicture || user.profilePicture}
              />
            </View>
            <Text style={styles.groupText}>
              {this.props.data.title || user.firstName}
            </Text>
            <Text style={styles.groupSubText}>
              {dist.distance} {dist.unit}
            </Text>
            <Caret
              style={[
                styles.caretIcon,
                {
                  transform: [
                    {
                      rotate: this.props.activeSections.length
                        ? '90deg'
                        : '0deg',
                    },
                  ],
                },
              ]}
              color={Colors.dark}
              height={16}
              width={32}
            />
          </View>
        </TouchableWithoutFeedback>
      </SwipeRow>
    );

    if (this.props.isHidden) {
      return (
        <AnimationHeight animation={{opacity: [1, 0], height: [64, 0]}}>
          {row}
        </AnimationHeight>
      );
    }
    return row;
  }
}

export default connect(mapStateToProps)(GroupRow);

const styles = StyleSheet.create({
  groupLine: {
    height: 64,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.light,
    borderBottomWidth: 1,
  },
  groupPicture: {
    paddingLeft: 20,
    width: '20%',
  },
  groupText: {
    width: '50%',
  },
  groupSubText: {
    width: '20%',
  },
  caretIcon: {
    width: '10%',
  },
  groupContent: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.light,
  },

  whiteFont: {
    color: Colors.white,
  },
});
