import React from 'react';
import {StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';
import {SwipeRow} from 'react-native-swipe-list-view';
import BackRow from '../components/backRow';
import {Colors} from '../../stylesVar';
import calculateDistance from '../../../services/distanceCalculator';
import {prettyDateAndHours} from '../../../services/dateConverter';
import * as Markers from '../../../assets/markers/includes';
const mapStateToProps = state => {
  return {
    user: state.user,
    unlockedList: state.unlockedList,
    messages: state.messages,
    conversations: state.conversations,
    currentPosition: state.currentPosition,
  };
};

class GroupContent extends React.Component {
  renderItem(item) {
    let logo;
    let isUserCreator = this.props.user.id === item.created_by.id;
    let isLocked = !this.props.unlockedList.includes(item.id);
    if (isUserCreator) {
      logo = isLocked ? <Markers.SeLoRe /> : <Markers.SeUnRe />;
    } else {
      logo = isLocked ? <Markers.ReLoRe /> : <Markers.ReUnRe />;
    }

    let date = prettyDateAndHours(item.created_at);
    let dist = calculateDistance(this.props.currentPosition, item);
    let messages = this.props.messages.filter(
      m => m.conversation.id === item.id,
    );
    let lastMessage =
      messages.length > 0 ? messages[messages.length - 1].message : 'ToFix';

    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.clickOnConversation(item)}>
        <View style={styles.container}>
          <View style={styles.messageIcon}>{logo}</View>
          <View style={styles.infoContainer}>
            <View style={styles.headLine}>
              <Text>{date}</Text>
              <Text>
                {dist.distance} {dist.unit}
              </Text>
            </View>
            <Text numberOfLines={2}>{lastMessage}</Text>
          </View>
          <View style={styles.dashedLine} />
        </View>
      </TouchableWithoutFeedback>
    );
  }

  render() {
    let conversationsOfGroup = this.props.conversations.filter(
      c => c.group.id === this.props.data.id,
    );
    return (
      <View style={styles.groupContent}>
        {conversationsOfGroup.map((c, index) => {
          return (
            <SwipeRow leftOpenValue={150} rightOpenValue={-75} key={index}>
              <BackRow />
              {this.renderItem(c)}
            </SwipeRow>
          );
        })}
      </View>
    );
  }
}

export default connect(mapStateToProps)(GroupContent);

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.light,
    display: 'flex',
    flexDirection: 'row',
    height: 64,
    alignItems: 'center',
  },
  messageIcon: {
    alignItems: 'center',
    flex: 0.15,
  },
  infoContainer: {
    flex: 0.85,
    marginRight: 12,
  },
  headLine: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  dashedLine: {
    position: 'absolute',
    width: '100%',
    height: 0,
    bottom: -5,
    borderColor: Colors.dark,
    borderRadius: 1,
    borderWidth: 3,
    borderStyle: 'dotted',
  },
});
