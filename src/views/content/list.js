import React from 'react';
import {StyleSheet, View} from 'react-native';
import ListGroups from './partials/listGroups';
import SortController from './components/sortController';
import {connect} from 'react-redux';

export class List extends React.Component {
  state = {
    sortDate: 0,
  };

  componentDidMount(): void {
    this.props.dispatch({
      type: 'SET_IS_MAP_VIEW_VISIBLE',
      value: false,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <SortController />
        <ListGroups navigation={this.props.navigation} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
});

export default connect()(List);
