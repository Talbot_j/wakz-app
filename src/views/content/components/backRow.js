import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors} from '../../stylesVar';
import Map from '../../../assets/icons/map.svg';
import User from '../../../assets/icons/user.svg';

export class BackRow extends React.Component {
  render() {
    return (
      <View style={styles.row}>
        <View style={styles.leftContainer}>
          <View style={styles.userButton}>
            <User />
          </View>
          <View style={styles.mapButton}>
            <Map color={Colors.accent} />
          </View>
        </View>
        <View style={styles.deleteButton}>
          <Text style={styles.whiteFont}>Delete</Text>
        </View>
      </View>
    );
  }
}

export default BackRow;

const styles = StyleSheet.create({
  row: {
    alignItems: 'center',
    backgroundColor: Colors.light,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
  },
  leftContainer: {
    flexDirection: 'row',
    width: 150,
    height: '100%',
  },
  userButton: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.light,
  },
  mapButton: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.base,
  },
  deleteButton: {
    backgroundColor: Colors.accent,
    height: '100%',
    width: 75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  whiteFont: {
    color: Colors.white,
  },
});
