import React from 'react';
import {connect} from 'react-redux';
import {Colors} from '../../stylesVar';
import {StyleSheet, View, Text} from 'react-native';
import SelectorIcon from '../../../components/selectorIcon';
import Calendar from '../../../assets/icons/calendar.svg';
import Distance from '../../../assets/icons/distance.svg';

const mapStateToProps = state => {
  return {
    sortByDate: state.sortByDate,
  };
};

export class SortController extends React.Component {
  setSortBy(sortBy) {
    this.props.dispatch({
      type: 'SET_SORT_BY_DATE',
      value: sortBy,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {this.props.sortByDate === 1 ? 'Date' : 'Distance'}
        </Text>
        <SelectorIcon
          icon={Distance}
          selected={this.props.sortByDate === 0}
          onPress={() => this.setSortBy(0)}
        />
        <SelectorIcon
          icon={Calendar}
          selected={this.props.sortByDate === 1}
          onPress={() => this.setSortBy(1)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 32,
    paddingRight: 64,
    height: 72,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.light,
    borderBottomWidth: 2,
  },
  text: {
    marginRight: 'auto',
  },
});

export default connect(mapStateToProps)(SortController);
