import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import * as Markers from '../../../assets/markers/includes';

import {Marker} from 'react-native-maps';
import {connect} from 'react-redux';
import {Colors} from '../../stylesVar';

const mapStateToProps = state => {
  return {
    user: state.user,
    unlockedList: state.unlockedList,
  };
};

export class WaxCluster extends React.Component {
  render() {
    let logo = <Markers.SeLoRe height={30} width={30} />;
    let type = this.props.markers.reduce((accumulator, currentValue) => {
      if (accumulator === 'ACCENT') {
        return accumulator;
      }
      if (this.props.user.id !== currentValue.properties.data.created_by.id) {
        return 'ACCENT';
      }
      return 'BASE';
    });
    if (type === 'ACCENT') {
      logo = <Markers.ReLoRe height={30} width={30} />;
    }
    return (
      <Marker
        anchor={{x: 0.5, y: 0.5}}
        coordinate={{
          longitude: this.props.geometry.coordinates[0],
          latitude: this.props.geometry.coordinates[1],
        }}
        onPress={this.props.onPress}>
        <View style={styles.cluster}>
          {logo}
          <Text style={styles.clusterText}>{this.props.markers.length}</Text>
        </View>
      </Marker>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  newWaxMessage: {
    position: 'absolute',
    bottom: 12,
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  cluster: {
    height: 30,
    width: 30,
  },
  clusterText: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    color: Colors.white,
    textAlign: 'center',
    marginTop: 4,
  },
});

export default connect(mapStateToProps)(WaxCluster);
