import React from 'react';
import {StyleSheet, View} from 'react-native';
import * as Markers from '../../../assets/markers/includes';

import {Marker} from 'react-native-maps';
import {connect} from 'react-redux';
import {Colors, FontsSize} from '../../stylesVar';
import calculateDistance from '../../../services/distanceCalculator';
import {
  getUnlockedList,
  postUnlocked,
} from '../../../services/apiCalls/conversation';
import {prettyDate} from '../../../services/dateConverter';
import MyCallout from '../../../components/markers/myCallout';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
    currentPosition: state.currentPosition,
    conversations: state.conversations,
    user: state.user,
    contacts: state.contacts,
    groups: state.groups,
    unlockedList: state.unlockedList,
  };
};

export class WaxMarker extends React.Component {
  markerRef = React.createRef();

  // noinspection JSCheckFunctionSignatures
  componentDidUpdate(): void {
    if (this.props.activeMarker === this.props.data.id.toString()) {
      this.markerRef.current.showCallout();
    }
  }

  onClickOnCallout() {
    let isLocked = !this.props.unlockedList.includes(this.props.data.id);
    if (isLocked) {
      let dist = calculateDistance(
        this.props.data,
        this.props.currentPosition,
        true,
      );
      if (dist.distance > 1000) {
        // ToDo: Find a way to alert user on locked Wakz
        return;
      }
      postUnlocked(this.props.data.id, this.props.authorization).then(() => {
        getUnlockedList(this.props.authorization).then(unlocked => {
          this.props.dispatch({type: 'SET_UNLOCKED_LIST', value: unlocked});
        });
      });
    }
    this.props.redirectTo(this.props.data);
  }
  handleMarkerPress(event) {
    const markerID = event.nativeEvent.id;
    this.props.onSelectMarker(markerID);
  }

  render() {
    if (!this.props.unlockedList) {
      return null;
    }
    let logo;
    let isUserCreator = this.props.user.id === this.props.data.created_by.id;
    let isLocked = !this.props.unlockedList.includes(this.props.data.id);
    let dist = calculateDistance(this.props.data, this.props.currentPosition);
    let date = prettyDate(this.props.data.created_at);
    if (isUserCreator) {
      logo = isLocked ? <Markers.SeLoRe /> : <Markers.SeUnRe />;
    } else {
      logo = isLocked ? <Markers.ReLoRe /> : <Markers.ReUnRe />;
    }
    let group_id = this.props.data.group.id;
    let group = this.props.groups.find(g => g.id === group_id);
    let user_id =
      group.users.find(contact => contact !== this.props.user.id) ||
      this.props.user.id;
    let user = this.props.contacts.find(contact => contact.id === user_id);

    return (
      <Marker
        ref={this.markerRef}
        identifier={this.props.data.id.toString()}
        onPress={e => this.handleMarkerPress(e)}
        anchor={{x: 0.5, y: 0.5}}
        key={this.props.key}
        coordinate={this.props.coordinate}
        description={this.props.comments}
        style={styles.container}>
        <View style={styles.marker}>{logo}</View>
        <MyCallout
          date={date}
          distance={dist}
          onClickOnCallout={() => this.onClickOnCallout()}
          title={group.title || (user ? user.firstName : null)}
          picture={group.picture || (user ? user.profilePicture : null)}
        />
      </Marker>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  groupText: {
    fontSize: FontsSize.base,
  },
  panel: {
    display: 'flex',
    flexDirection: 'row',
    width: 200,
    height: 60,
    backgroundColor: Colors.accent,
    borderRadius: 5,
    alignItems: 'center',
  },
  pictureContainer: {
    marginLeft: 10,
    marginRight: 10,
  },

  marker: {
    height: 20,
    width: 20,
  },
});

export default connect(mapStateToProps)(WaxMarker);
