import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import {Colors} from '../../stylesVar';
import Plus from '../../../assets/icons/plus-circled.svg';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    isSettingsViewVisible: state.isSettingsViewVisible,
  };
};

class OverlayButtonNew extends Component {
  sendMessage() {
    this.props.navigation.navigate('NewWax');
  }

  render() {
    if (this.props.isSettingsViewVisible) {
      return null;
    }
    return (
      <TouchableOpacity
        style={styles.newMessageButton}
        onPress={() => this.sendMessage()}>
        <Plus color={Colors.accent} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  newMessageButton: {
    position: 'absolute',
    height: 75,
    width: 75,
    bottom: 12,
    left: (Dimensions.get('screen').width - 75) / 2,
  },
});

export default connect(mapStateToProps)(OverlayButtonNew);
