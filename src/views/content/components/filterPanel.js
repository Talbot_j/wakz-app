import React from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import SlidingPanel from 'react-native-sliding-panels';
import Funnel from '../../../assets/icons/funnel.svg';
import {Colors} from '../../stylesVar';
import SelectorOneFromMultiple from '../../../components/selectorOneFromMultiple';
import SelectorListUser from '../../../components/selectorListUser';
const {width, height} = Dimensions.get('window');

const mapStateToProps = state => {
  return {
    isSettingsViewVisible: state.isSettingsViewVisible,
  };
};

export class FilterPanel extends React.Component {
  state = {
    selectedUser: [],
    selectedRead: 0,
  };

  render() {
    if (this.props.isSettingsViewVisible) {
      return null;
    }
    return (
      <SlidingPanel
        panelPosition={'right'}
        allowDragging={true}
        AnimationSpeed={100}
        headerLayoutWidth={42}
        slidingPanelLayoutWidth={(width / 100) * 60}
        headerLayout={() => (
          <View style={styles.headerLayoutStyle}>
            <Funnel color={Colors.accent} />
          </View>
        )}
        slidingPanelLayout={() => (
          <View style={styles.slidingPanelLayoutStyle}>
            <SelectorOneFromMultiple
              selected={this.state.selectedRead}
              selection={['Unread', 'Read']}
              onChangeSelected={s => this.setState({selectedRead: s})}
            />
            <SelectorListUser
              selected={this.state.selectedUser}
              onSelectUser={s => this.setState({selectedUser: s})}
            />
          </View>
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerLayoutStyle: {
    top: 87,
    width: 42,
    height: 42,
    backgroundColor: Colors.base,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slidingPanelLayoutStyle: {
    width: width * 0.6,
    height,
    backgroundColor: Colors.base,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commonTextStyle: {
    color: 'white',
    fontSize: 18,
  },
});

export default connect(mapStateToProps)(FilterPanel);
