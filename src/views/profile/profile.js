import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux';
import {postProfileInfo} from '../../services/apiCalls/auth';
import ImagePickerCropper from '../../components/imagePickerCropper';
import InputText from '../../components/inputText';
import ButtonTextBackground from '../../components/buttonTextBackground';
import {Colors, FontsSize} from '../stylesVar';
import ButtonBack from '../../components/buttonBack';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
    user: state.user,
  };
};

class Profile extends Component {
  state = {
    firstName: this.props.user.firstName,
    profilePicture: this.props.user.profilePicture,
  };

  onChangeName(firstName) {
    this.setState({
      firstName: firstName,
    });
  }
  onChangePicture(profilePicture) {
    this.setState({
      profilePicture: profilePicture,
    });
  }

  sendProfileInfo() {
    postProfileInfo(
      this.state.firstName,
      this.state.profilePicture,
      this.props.authorization,
    ).then(
      success => {
        this.props.dispatch({type: 'SET_USER', value: success});
        this.props.dispatch({type: 'SET_REGISTER_COMPLETE', value: true});
      },
      error => {
        this.console.log('todo: warn error on postUserInfos', error);
      },
    );
  }

  onReturnButton() {
    if (!this.props.user.firstName) {
      this.props.dispatch({type: 'SET_DISCONNECTED', value: true});
    } else {
      this.props.dispatch({type: 'SET_REGISTER_COMPLETE', value: true});
    }
  }

  render() {
    return (
      <View style={styles.content}>
        <ButtonBack
          callBack={() => this.onReturnButton()}
          absolutePosition={true}
        />
        <Text style={styles.title}>
          {this.props.user.firstName ? 'EDIT PROFILE' : 'CREATE PROFILE'}
        </Text>
        <ImagePickerCropper
          callBack={picture => this.onChangePicture(picture)}
          value={this.state.profilePicture}
        />
        <InputText
          callBack={firstName => this.onChangeName(firstName)}
          value={this.state.firstName}
          placeholder={'Name'}
          length={12}
        />
        <ButtonTextBackground
          callBack={() => this.sendProfileInfo()}
          text={'Next'}
          disabled={!this.state.firstName}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginTop: '8%',
    marginLeft: 'auto',
    marginRight: 'auto',
    color: Colors.accent,
    fontSize: FontsSize.h5,
  },
});

export default connect(mapStateToProps)(Profile);
