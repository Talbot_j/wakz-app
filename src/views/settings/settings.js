import React from 'react';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import ButtonIconText from '../../components/buttonIconText';
import User from '../../assets/icons/user-background.svg';
import {Colors} from '../stylesVar';

export class Settings extends React.Component {
  componentDidMount(): void {
    this.props.dispatch({
      type: 'SET_IS_SETTINGS_VIEW_VISIBLE',
      value: true,
    });
  }

  disconnect() {
    this.props.dispatch({type: 'SET_DISCONNECTED', value: true});
  }

  editProfile() {
    this.props.dispatch({type: 'SET_REGISTER_COMPLETE', value: false});
  }

  render() {
    return (
      <View style={styles.container}>
        <ButtonIconText
          icon={User}
          title={'Profile'}
          onPress={() => this.editProfile()}
        />
        <ButtonIconText
          icon={User}
          title={'Disconnect'}
          onPress={() => this.disconnect()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.light,
    alignItems: 'center',
  },
});

export default connect()(Settings);
