export const Colors = {
  dark: '#000000', // Colors.dark
  base: '#14213D', // Colors.base
  accent: '#FCA311', // Colors.accent
  light: '#DADADA', // Colors.light
  white: '#FFFFFF', // Colors.white
};

export const FontsSize = {
  h1: 40, // Fonts.h1
  h2: 36, // Fonts.h2
  h3: 32, // Fonts.h3
  h4: 28, // Fonts.h4
  h5: 24, // Fonts.h5
  h6: 20, // Fonts.h6
  base: 16, // Fonts.base
  little: 14, // Fonts.little
  tiny: 12, // Fonts.tiny
  mini: 8, // Fonts.mini
};

export function LoginInput(margin = 15) {
  return {
    marginLeft: margin + '%',
    marginRight: margin + '%',
    height: 36,
    backgroundColor: Colors.white,
    borderColor: Colors.dark,
    borderRadius: 12,
  };
}

export function FontCentered(margin = 10) {
  return {
    textAlign: 'center',
    marginLeft: margin + '%',
    marginRight: margin + '%',
  };
}
