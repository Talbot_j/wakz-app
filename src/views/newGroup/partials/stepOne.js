import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Colors, FontsSize} from '../../stylesVar';
import {View, Text} from 'react-native';
import AddUser from '../../../assets/icons/user-plus.svg';
import SelectorListUser from '../../../components/selectorListUser';
import SelectorListUserRoundedDeletable from '../../../components/selectorListUserRoundedDeletable';
import SearchBar from '../../../components/searchBar';

class StepOne extends Component {
  createUser() {}

  render() {
    let createView = null;
    if (this.props.selected.length === 0) {
      createView = (
        <View>
          <TouchableOpacity
            onPress={() => this.createUser()}
            style={styles.itemContainer}>
            <View style={styles.whiteBackground}>
              <AddUser style={styles.accentIcon} />
            </View>
            <Text style={styles.itemText}>New User</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <View>
        <SearchBar />
        {createView}
        <SelectorListUserRoundedDeletable
          users={this.props.selected}
          onDeleteUser={s => this.props.setSelected(s)}
        />
        <SelectorListUser
          contactOnly={true}
          selected={this.props.selected}
          onSelectUser={s => this.props.setSelected(s)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    paddingTop: 24,
    paddingLeft: 24,
    paddingRight: 24,
    height: 52,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemText: {
    marginLeft: 24,
    marginRight: 'auto',
    fontSize: FontsSize.base,
    color: Colors.accent,
  },
  whiteBackground: {
    backgroundColor: Colors.white,
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  accentIcon: {
    color: Colors.accent,
  },
});

export default StepOne;
