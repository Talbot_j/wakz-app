import React, {Component} from 'react';
import {View, Keyboard, TouchableWithoutFeedback} from 'react-native';
import ImagePickerCropper from '../../../components/imagePickerCropper';
import InputText from '../../../components/inputText';
import SelectorListUserDeletable from '../../../components/selectorListUserDeletable';

class StepTwo extends Component {
  onPickName = text => {
    this.props.setTitle(text);
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View>
          <ImagePickerCropper
            callBack={picture => this.props.onChangePicture(picture)}
            value={this.props.profilePicture}
          />
          <InputText
            callBack={this.onPickName}
            value={this.props.title}
            placeholder={'Name'}
            length={12}
          />
          <SelectorListUserDeletable
            users={this.props.usersList}
            onDeleteUser={list => this.props.setSelected(list)}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default StepTwo;
