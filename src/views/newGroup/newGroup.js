import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../stylesVar';

import StepOne from './partials/stepOne';
import StepTwo from './partials/stepTwo';
import {getGroup, postGroup} from '../../services/apiCalls/conversation';
import {connect} from 'react-redux';
import Header from '../../components/header';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
    currentPosition: state.currentPosition,
  };
};

export class NewGroup extends Component {
  state = {
    selectedUsers: [],
    title: '',
    picture: null,
    step: 1,
  };

  setPicture(picture) {
    this.setState({
      picture: picture,
    });
  }
  setSelected(selection) {
    this.setState({
      selectedUsers: selection,
    });
  }
  setTitle(title) {
    this.setState({
      title: title,
    });
  }
  setStep(step) {
    if (step === 2 && !this.state.selectedUsers.length) {
      return;
    }
    this.setState({
      step: step,
    });
  }
  sendGroup() {
    let dests = [];
    this.state.selectedUsers.forEach(u => {
      dests.push(u.id);
    });
    postGroup(
      dests,
      this.state.title,
      this.state.picture,
      this.props.authorization,
    ).then(() => {
      getGroup(this.props.authorization).then(groups => {
        this.props.dispatch({type: 'SET_GROUPS', value: groups});
      });
      this.props.navigation.pop();
    });
  }

  render() {
    let view = {
      header: null,
      content: null,
    };
    switch (this.state.step) {
      case 1:
        view.header = (
          <Header
            title={'New Group'}
            nextTitle={'Next'}
            callBack={() => this.props.navigation.pop()}
            callNext={() => this.setStep(2)}
          />
        );
        view.content = (
          <StepOne
            navigation={this.props.navigation}
            selected={this.state.selectedUsers}
            setSelected={s => this.setSelected(s)}
          />
        );
        break;
      case 2:
        view.header = (
          <Header
            title={'New Group'}
            nextTitle={'Create'}
            callBack={() => this.setStep(1)}
            callNext={() => this.sendGroup()}
          />
        );
        view.content = (
          <StepTwo
            title={this.state.title}
            setTitle={t => this.setTitle(t)}
            usersList={this.state.selectedUsers}
            setSelected={s => this.setSelected(s)}
            onChangePicture={picture => this.setPicture(picture)}
            profilePicture={this.state.picture}
          />
        );
        break;
      default:
        break;
    }
    return (
      <View style={styles.container}>
        {view.header}
        <View style={styles.content}>{view.content}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.base,
    alignItems: 'center',
  },
  content: {
    width: '100%',
    height: '85%',
  },
});

export default connect(mapStateToProps)(NewGroup);
