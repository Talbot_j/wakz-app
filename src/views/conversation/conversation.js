import React from 'react';
import {StyleSheet, View, KeyboardAvoidingView, Platform} from 'react-native';
import {connect} from 'react-redux';
import Header from './partials/header';
import ListMessages from './partials/listMessages';
import InputChat from './partials/inputChat';
import {Colors} from '../stylesVar';

const mapStateToProps = state => {
  return {
    user: state.user,
    contacts: state.contacts,
    selectedConversation: state.selectedConversation,
    selectedConversationGroup: state.selectedConversationGroup,
    selectedConversationMessages: state.selectedConversationMessages,
  };
};

export class Conversation extends React.Component {
  render() {
    let title = this.props.selectedConversationGroup.title;
    if (!title) {
      let user_id = this.props.selectedConversationGroup.users.find(
        id => id !== this.props.user.id,
      );
      title = user_id
        ? this.props.contacts.find(contact => contact.id === user_id).firstName
        : this.props.user.firstName;
    }

    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          style={styles.avoidingView}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 48 : 0}
          behavior={Platform.OS === 'ios' ? 'padding' : ''}>
          <Header back={() => this.props.navigation.pop()} title={title} />
          <ListMessages
            data={this.props.selectedConversationMessages}
            userId={this.props.user.id}
          />
          <InputChat conversationId={this.props.selectedConversation.id} />
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default connect(mapStateToProps)(Conversation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.light,
  },
  avoidingView: {
    flex: 1,
  },
});
