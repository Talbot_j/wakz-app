import React, {Component} from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import {Colors} from '../../stylesVar';

class InputText extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          multiline
          numberOfLines={2}
          maxLength={this.props.length}
          placeholder={this.props.placeholder}
          underlineColorAndroid="transparent"
          onChangeText={value => this.props.callBack(value)}
          value={this.props.value}
          style={styles.transparentInput}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    minHeight: 36,
    maxHeight: 70,
    backgroundColor: Colors.accent + '55',
    borderColor: Colors.accent,
    borderWidth: 1,
    width: '60%',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  transparentInput: {
    padding: 0,
    width: '90%',
    color: Colors.base,
  },
});

export default InputText;
