import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Caret from './../../../assets/icons/caret.svg';
import {Colors, FontsSize} from '../../stylesVar';

export default class Header extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.left} onPress={() => this.props.back()}>
          <Caret
            width={26}
            height={26}
            style={{transform: [{rotate: '180deg'}], color: Colors.accent}}
          />
        </TouchableOpacity>
        <Text style={styles.title}>{this.props.title}</Text>
        <View style={styles.right} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: Colors.base,
    alignItems: 'center',
    justifyContent: 'center',
    height: 74,
  },
  left: {
    marginRight: 'auto',
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: Colors.accent,
    fontSize: FontsSize.base,
  },
  right: {
    marginLeft: 'auto',
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
