import React, {Component} from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {Colors, FontsSize} from '../../stylesVar';

export default class ListMessages extends Component {
  renderItem = ({item /* , index */}) => {
    return (
      <TouchableWithoutFeedback onPress={() => null}>
        <View
          style={[
            styles.itemContainer,
            item.created_by.id === this.props.userId
              ? styles.messageSent
              : styles.messageReceived,
          ]}>
          <Text style={styles.itemText}>{item.message}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    return (
      <FlatList
        inverted={-1}
        style={styles.list}
        data={this.props.data.reverse()}
        renderItem={item => this.renderItem(item)}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

const styles = StyleSheet.create({
  list: {
    height: '100%',
    paddingLeft: 12,
    paddingRight: 12,
  },
  itemContainer: {
    borderRadius: 12,
    borderWidth: 2,
    marginTop: 1,
    marginBottom: 1,
  },
  messageSent: {
    marginLeft: 'auto',
    borderColor: Colors.base,
    backgroundColor: Colors.base + '88',
  },
  messageReceived: {
    marginRight: 'auto',
    borderColor: Colors.accent,
    backgroundColor: Colors.accent + '88',
  },
  itemText: {
    padding: 8,
    fontSize: FontsSize.base,
    color: Colors.base,
  },
});
