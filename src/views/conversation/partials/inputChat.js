import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Camera from '../../../assets/icons/camera.svg';
import Gallery from '../../../assets/icons/gallery.svg';
import Send from '../../../assets/icons/send.svg';
import Micro from '../../../assets/icons/micro.svg';
import {Colors} from '../../stylesVar';
import InputText from '../components/inputText';
import {getMessage, postMessage} from '../../../services/apiCalls/conversation';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
  };
};

export class InputChat extends React.Component {
  state = {
    messageText: '',
  };

  onMessageTextChange(text) {
    this.setState({
      messageText: text,
    });
  }

  sendMessage() {
    if (!this.state.messageText) {
      return;
    }
    postMessage(
      this.props.conversationId,
      this.state.messageText,
      this.props.authorization,
    ).then(() => {
      getMessage(this.props.authorization).then(messages => {
        this.props.dispatch({type: 'SET_MESSAGES', value: messages});
      });
      this.setState({
        messageText: '',
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <InputText
          value={this.state.messageText}
          callBack={t => this.onMessageTextChange(t)}
        />
        <TouchableOpacity
          style={styles.icon}
          onPress={() => this.sendMessage()}>
          <Send width={24} height={24} color={Colors.base} />
        </TouchableOpacity>
        <View style={styles.icon}>
          <Camera width={24} height={24} color={Colors.base} />
        </View>
        <View style={styles.icon}>
          <Gallery width={24} height={24} color={Colors.base} />
        </View>
        <View style={styles.icon}>
          <Micro width={24} height={24} color={Colors.base} />
        </View>
      </View>
    );
  }
}

export default connect(mapStateToProps)(InputChat);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingLeft: 6,
    paddingRight: 6,
    paddingTop: 12,
    paddingBottom: 12,
    flexDirection: 'row',
    backgroundColor: Colors.base,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  icon: {
    color: Colors.base,
    backgroundColor: Colors.accent,
    borderRadius: 50,
    padding: 6,
  },
});
