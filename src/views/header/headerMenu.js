import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Logo from '../../assets/logos/logo.svg';
import Settings from '../../assets/icons/gear.svg';
import Caret from '../../assets/icons/caret.svg';
import MapIcon from '../../assets/icons/map.svg';
import ListIcon from '../../assets/icons/list.svg';
import {Colors} from '../stylesVar';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    isSettingsViewVisible: state.isSettingsViewVisible,
    isMapViewVisible: state.isMapViewVisible,
  };
};

export class HeaderMenu extends React.Component {
  toggleMainView() {
    this.props.dispatch({
      type: 'SET_IS_MAP_VIEW_VISIBLE',
      value: !this.props.isMapViewVisible,
    });
    this.props.dispatch({
      type: 'SET_IS_SETTINGS_VIEW_VISIBLE',
      value: false,
    });
    this.props.navigation.navigate('MainView', {
      screen: this.props.isMapViewVisible === false ? 'Map' : 'List',
    });
  }
  navigateToSettings() {
    if (this.props.isSettingsViewVisible) {
      this.props.navigation.navigate('MainView', {
        screen: this.props.isMapViewVisible === true ? 'Map' : 'List',
      });
      this.props.dispatch({
        type: 'SET_IS_SETTINGS_VIEW_VISIBLE',
        value: false,
      });
    } else {
      this.props.navigation.navigate('MainView', {screen: 'Settings'});
    }
  }

  render() {
    let viewIcon =
      this.props.isMapViewVisible === false ? (
        <MapIcon style={styles.icon} />
      ) : (
        <ListIcon style={styles.icon} />
      );
    let settingsIcon =
      this.props.isSettingsViewVisible === false ? (
        <Settings style={styles.icon} />
      ) : (
        <Caret style={styles.icon} />
      );

    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.left}
          onPress={() => this.navigateToSettings()}>
          {settingsIcon}
        </TouchableOpacity>
        <Logo style={styles.logo} />
        <TouchableOpacity
          style={styles.right}
          onPress={() => this.toggleMainView()}>
          {viewIcon}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: Colors.base,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 12,
    paddingBottom: 12,
    zIndex: 1,
  },
  left: {
    marginRight: 'auto',
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
    transform: [{rotateY: '180deg'}],
  },
  right: {
    marginLeft: 'auto',
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 50,
    height: 50,
  },
  icon: {
    color: Colors.accent,
    width: 26,
    height: 26,
  },
});

export default connect(mapStateToProps)(HeaderMenu);
