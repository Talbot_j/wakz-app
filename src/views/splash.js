import React from 'react';
import {StyleSheet, View} from 'react-native';
import Logo from '../assets/logos/logo.svg';
import {Colors} from './stylesVar';

export default class Splash extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Logo style={styles.waxLogo} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.base,
  },
  logoContainer: {
    flex: 0.66,
    alignItems: 'center',
    justifyContent: 'center',
  },
  waxLogo: {
    height: '100%',
    width: '25%',
  },
});
