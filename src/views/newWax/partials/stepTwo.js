import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {Colors} from '../../stylesVar';

import UTurnArrow from '../../../assets/icons/uturn-arrow.svg';
import CameraContour from '../../../assets/icons/camera-contour.svg';
import CropTurn from '../../../assets/icons/crop-turn.svg';
import Pen from '../../../assets/icons/pen.svg';
import TextContour from '../../../assets/icons/text.svg';

import {SketchCanvas} from '@terrylinla/react-native-sketch-canvas';
import ButtonBack from '../../../components/buttonBack';
import SelectorIcon from '../../../components/selectorIcon';
import ButtonIcon from '../../../components/buttonIcon';
import {askCamera} from '../../../services/askPermission';
import ImagePicker from 'react-native-image-crop-picker';
import {Dimensions} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const options = {
  width: windowWidth,
  height: windowHeight,
  cropperCircleOverlay: false,
  showCropGuidelines: false,
  cropping: true,
  includeBase64: true,
};

class StepTwo extends Component {
  state = {
    photoPath: null,
    strokeColor: Colors.base,
    numberOfStroke: 0,
    strokeOn: false,
  };

  undo() {
    this.canvas.undo();
  }
  cameraOpen() {
    askCamera().then(granted => {
      if (granted) {
        ImagePicker.openCamera(options).then(response => {
          this.setState({
            photoPath: response.path.replace('file://', ''),
          });
        });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={styles.sketchContainer}>
            <SketchCanvas
              ref={ref => (this.canvas = ref)}
              style={styles.sketchCanvas}
              strokeColor={this.state.strokeColor}
              strokeWidth={7}
              localSourceImage={{
                filename: this.state.photoPath,
                directory: null,
                mode: 'AspectFit',
              }}
              onPathsChange={count => this.setState({numberOfStroke: count})}
            />
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.buttonBackContainer}>
          <ButtonBack
            size={20}
            color={Colors.base}
            callBack={() => this.props.callBack()}
            absolutePosition={false}
          />
        </View>
        <View style={styles.buttonContainer}>
          <ButtonIcon
            icon={UTurnArrow}
            color={Colors.white}
            hidden={!this.state.numberOfStroke}
            size={'18'}
            callBack={() => this.undo()}
          />
          <ButtonIcon
            icon={CameraContour}
            color={Colors.white}
            size={'24'}
            callBack={() => this.cameraOpen()}
          />
          <SelectorIcon
            icon={CropTurn}
            colorIcon={Colors.white}
            colorIconSelected={Colors.dark}
            colorBackground={'transparent'}
            selected={this.state.strokeOn}
            onPress={() => this.setState({strokeOn: !this.state.strokeOn})}
          />
          <SelectorIcon
            icon={TextContour}
            colorIcon={Colors.white}
            colorIconSelected={Colors.dark}
            colorBackground={'transparent'}
            selected={this.state.strokeOn}
            onPress={() => this.setState({strokeOn: !this.state.strokeOn})}
          />
          <SelectorIcon
            icon={Pen}
            colorIcon={Colors.white}
            colorIconSelected={Colors.dark}
            colorBackground={'transparent'}
            selected={this.state.strokeOn}
            onPress={() => this.setState({strokeOn: !this.state.strokeOn})}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sketchContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.accent,
  },
  sketchCanvas: {
    flex: 1,
  },
  buttonBackContainer: {
    position: 'absolute',
    left: 18,
    top: 18,
    backgroundColor: 'transparent',
  },
  buttonContainer: {
    position: 'absolute',
    right: 0,
    alignItems: 'center',
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
});

export default StepTwo;
