import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../stylesVar';

import Header from '../../components/header';
import StepOne from './partials/stepOne';
import StepTwo from './partials/stepTwo';
import {
  getConversation,
  getGroup,
  postWax,
} from '../../services/apiCalls/conversation';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
    currentPosition: state.currentPosition,
  };
};

export class NewWax extends Component {
  state = {
    selectedUsers: [],
    message: '',
    step: 1,
  };

  setSelected(selection) {
    this.setState({
      selectedUsers: selection,
    });
  }
  setMessage(message) {
    this.setState({
      message: message,
    });
  }
  setStep(step) {
    if (step === 2 && !this.state.selectedUsers.length) {
      return;
    }
    this.setState({
      step: step,
    });
  }

  selectedUsersToTitle(userList) {
    let title = '';
    userList.forEach((s, index) => {
      title += s.firstName || s.title;
      if (index < userList.length - 1) {
        title += ', ';
      }
    });
    return title;
  }
  selectedUsersToJSON(userList = this.state.selectedUsers) {
    let result = {
      groups: [],
      users: [],
    };
    userList.forEach(u => {
      if (u.title) {
        result.groups.push(u.id);
      } else {
        result.users.push(u.id);
      }
    });
    return result;
  }

  sendWax() {
    if (!this.state.message) {
      return false;
    }
    postWax(
      this.selectedUsersToJSON(),
      this.state.message,
      this.props.currentPosition,
      this.props.authorization,
    ).then(() => {
      getConversation(this.props.authorization).then(conversations => {
        this.props.dispatch({
          type: 'SET_CONVERSATIONS',
          value: conversations,
        });
      });
      getGroup(this.props.authorization).then(group => {
        this.props.dispatch({type: 'SET_GROUPS', value: group});
      });
      this.props.navigation.pop();
    });
  }

  render() {
    let view = {
      header: null,
      content: null,
    };
    switch (this.state.step) {
      case 1:
        view.header = (
          <Header
            title={'New WAX'}
            nextTitle={'Next'}
            callBack={() => this.props.navigation.pop()}
            callNext={() => this.setStep(2)}
          />
        );
        view.content = (
          <StepOne
            navigation={this.props.navigation}
            selected={this.state.selectedUsers}
            setSelected={s => this.setSelected(s)}
          />
        );
        break;
      case 2:
        view.content = (
          <StepTwo
            callBack={() => this.setStep(1)}
            callNext={() => this.sendWax()}
            message={this.state.message}
            setMessage={msg => this.setMessage(msg)}
          />
        );
        break;
      default:
        break;
    }
    return (
      <View style={styles.container}>
        {view.header}
        <View style={styles.content}>{view.content}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '105%',
    backgroundColor: Colors.base,
    alignItems: 'center',
  },
  content: {
    width: '100%',
    flex: 1,
  },
});

export default connect(mapStateToProps)(NewWax);
