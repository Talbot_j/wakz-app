import React from 'react';
import {connect} from 'react-redux';

import Home from './views/home';
import Auth from './views/auth/auth';
import {askContact} from './services/askPermission';
import Contacts from 'react-native-contacts';
import {getContactsInfos, postContactsInfos} from './services/apiCalls/users';
import {
  getGroup,
  getMessage,
  getUnlockedList,
  getConversation,
} from './services/apiCalls/conversation';
import Profile from './views/profile/profile';

const mapStateToProps = state => {
  return {
    authorization: state.authorization,
    isRegisterComplete: state.isRegisterComplete,
    user: state.user,
  };
};

export class AppWithStore extends React.Component {
  getContact() {
    askContact().then(() => {
      Contacts.getAll((androidContactPermission, contacts) => {
        let phoneNumbers = [];
        contacts.forEach(contact => {
          if (contact.phoneNumbers.length >= 1) {
            phoneNumbers.push(contact.phoneNumbers[0].number);
          }
        });
        postContactsInfos(
          phoneNumbers,
          this.props.authorization,
          this.props.dispatch,
        ).then(() => {
          getContactsInfos(this.props.authorization, this.props.dispatch).then(
            contactsInfos => {
              this.props.dispatch({type: 'SET_CONTACTS', value: contactsInfos});
            },
          );
        });
      });
    });
  }

  getConversation() {
    getConversation(this.props.authorization, this.props.dispatch).then(
      conversations => {
        this.props.dispatch({type: 'SET_CONVERSATIONS', value: conversations});
      },
    );
  }
  getGroup() {
    getGroup(this.props.authorization, this.props.dispatch).then(groups => {
      this.props.dispatch({type: 'SET_GROUPS', value: groups});
    });
  }
  getMessage() {
    getMessage(this.props.authorization, this.props.dispatch).then(messages => {
      this.props.dispatch({type: 'SET_MESSAGES', value: messages});
    });
  }
  getUnlockedList() {
    getUnlockedList(this.props.authorization, this.props.dispatch)
      .then(unlockedList => {
        this.props.dispatch({type: 'SET_UNLOCKED_LIST', value: unlockedList});
      })
      .catch(error => {
        console.warn('UnlockedListError', error);
      });
  }

  initApp() {
    this.getContact();
    this.getGroup();
    this.getConversation();
    this.getMessage();
    this.getUnlockedList();
  }

  render() {
    if (!this.props.authorization) {
      return <Auth />;
    } else if (!this.props.user.firstName || !this.props.isRegisterComplete) {
      return <Profile />;
    }
    this.initApp();
    return <Home />;
  }
}

export default connect(mapStateToProps)(AppWithStore);
