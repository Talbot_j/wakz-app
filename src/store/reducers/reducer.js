const initialState = {
  isRegisterComplete: false,
  authorization: '',
  currentPosition: {
    longitude: null,
    latitude: null,
  },
  user: {
    id: '',
    firstName: '',
    profilePicture: {
      mime: '',
      data: '',
    },
  },
  contacts: [],
  groups: [],
  conversations: [],
  messages: [],
  unlockedList: [],

  // ToDo: remove persistent for next values
  selectedConversation: null,
  selectedConversationGroup: null,
  selectedConversationMessages: [],
  sortByDate: 0,
  isSettingsViewVisible: false,
  isMapViewVisible: true,
};

export default function setUser(state = initialState, action) {
  let nextState;
  switch (action.type) {
    case 'SET_AUTHORIZATION':
      nextState = {
        ...state,
        authorization: action.value,
      };
      return nextState;
    case 'SET_USER':
      nextState = {
        ...state,
        user: {...state.user, ...action.value},
      };
      return nextState;
    case 'SET_REGISTER_COMPLETE':
      nextState = {
        ...state,
        isRegisterComplete: action.value,
      };
      return nextState;
    case 'SET_CONTACTS':
      nextState = {
        ...state,
        contacts: [...action.value],
      };
      return nextState;
    case 'SET_GROUPS':
      nextState = {
        ...state,
        groups: [...action.value],
      };
      return nextState;
    case 'SET_CONVERSATIONS':
      nextState = {
        ...state,
        conversations: [...action.value],
      };
      return nextState;
    case 'SET_MESSAGES':
      let s_messages = [];
      if (state.selectedConversation) {
        s_messages = action.value.filter(
          m => m.conversation.id === state.selectedConversation.id,
        );
      }
      nextState = {
        ...state,
        messages: [...action.value],
        selectedConversationMessages: s_messages,
      };
      return nextState;
    case 'SET_CURRENT_POSITION':
      nextState = {
        ...state,
        currentPosition: action.value,
      };
      return nextState;
    case 'SET_SELECTED_CONVERSATION':
      let group = state.groups.find(g => g.id === action.value.group.id);
      let messages = state.messages.filter(
        m => m.conversation.id === action.value.id,
      );

      nextState = {
        ...state,
        selectedConversation: action.value,
        selectedConversationGroup: group,
        selectedConversationMessages: messages,
      };
      return nextState;
    case 'SET_UNLOCKED_LIST':
      nextState = {
        ...state,
        unlockedList: action.value,
      };
      return nextState;
    case 'SET_SORT_BY_DATE':
      nextState = {
        ...state,
        sortByDate: action.value,
      };
      return nextState;
    case 'SET_IS_SETTINGS_VIEW_VISIBLE':
      nextState = {
        ...state,
        isSettingsViewVisible: action.value,
      };
      return nextState;
    case 'SET_IS_MAP_VIEW_VISIBLE':
      nextState = {
        ...state,
        isMapViewVisible: action.value,
      };
      return nextState;
    case 'SET_DISCONNECTED':
      nextState = initialState;
      return nextState;
    default:
      break;
  }
  return state;
}
