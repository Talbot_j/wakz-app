import {createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';

import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

import setUser from './reducers/reducer';

import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
};

const pReducer = persistReducer(persistConfig, setUser);

export const store = createStore(pReducer);
export const persistor = persistStore(store);
