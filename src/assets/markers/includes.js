// export SeUnNe from './dotted-accent.svg'; // 'New Message on 'Unlocked Wax Sended
// export ReUnNe from './dotted-base.svg'; // 'New Message on 'Unlocked Wax Received
export SeUnRe from './empty-accent.svg'; // 'Read Message on 'Unlocked Wax Sended
export ReUnRe from './empty-base.svg'; // 'Read Message on 'Unlocked Wax Received
export SeLoRe from './plain-accent.svg'; // 'Read Message on 'Locked Wax Sended
export ReLoRe from './plain-base.svg'; // 'Read Message on 'Locked Wax Received

// export ToDefined from "/dotted-accent-base.svg";
// export ToDefined from "/dotted-base-accent.svg";
